@extends('layouts.main')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Tambah SKPD</h1>
            </div>
            <div class="section-body">
                {{-- <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p> --}}
                <div class="row">
                    <div class="col-8 mx-auto">
                        <div class="card">
                            <div class="card-header">
                                <h4>Tambah Data SKPD</h4>
                            </div>


                            <div class="card-body">
                                <div class="card-body">
                                    <div class="form-group">
                                        <label>Nama SKPD</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Kode SKPD</label>
                                        <input type="text" class="form-control">
                                    </div>
                                    <div class="col-sm-12 col-md-7">
                                        <button class="btn btn-primary">Simpan</button>
                                    </div>

                                </div>
                            </div>


                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
