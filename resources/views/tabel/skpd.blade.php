@extends('layouts.main')
@section('judul', 'Data Skpd')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data SKPD</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-primary rounded-1" data-toggle="modal" data-target="#exampleModal">
                                    Import Excel
                                </button>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-biasa">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Unit Key</th>
                                                <th>Kode Unit</th>
                                                <th>Nama SKPD</th>
                                                <th>Level SKPD</th>
                                                <th>Type</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach ($skpds as $row)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $row->unit_key }}</td>
                                                    <td>{{ $row->kode_skpd }}</td>
                                                    <td>{{ $row->nm_skpd }}</td>
                                                    <td>{{ $row->lv_id }}</td>
                                                    <td>{{ $row->type }}</td>
                                                    <td style="width: 350px">
                                                        <div class="d-inline-block">
                                                            <a href="{{ route('dataskpd.edit', $row->id) }}"
                                                                class="btn btn-warning"> <i class="fas fa-eye"></i>
                                                                Edit</a>
                                                        </div>
                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/importskpd" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-body">
                            <div class="form-group">
                                <input type="file" name="file" required>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                </div>
                </form>
            </div>
        </div>

        {{-- modalTambah --}}
        <div class="modal fade" role="dialog" id="ModalTambah">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah Data SKPD</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <form method="POST" action="{{ route('dataskpd.store') }}" class="needs-validation">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="unit_key">Unit Key</label>
                                    <input id="unit_key" type="text"
                                        class="@error('unit_key') is-invalid @enderror form-control" name="unit_key"
                                        tabindex="1" value="{{ old('unit_key') }}" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="kode_skpd">Kode SKPD</label>
                                    <input id="kode_skpd" type="text"
                                        class="@error('kode_skpd') is-invalid @enderror form-control" name="kode_skpd"
                                        tabindex="1" value="{{ old('kode_skpd') }}" required autofocus>
                                    @error('kode_skpd')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="nm_skpd">Nama SKPD</label>
                                    <input id="nm_skpd" type="text"
                                        class="@error('nm_skpd') is-invalid @enderror form-control" name="nm_skpd"
                                        tabindex="1" value="{{ old('nm_skpd') }}" required autofocus>
                                    @error('nm_skpd')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="lv_id">Id Level</label>
                                    <input id="lv_id" type="text"
                                        class="@error('lv_id') is-invalid @enderror form-control" name="lv_id"
                                        tabindex="1" value="{{ old('lv_id') }}" required autofocus>
                                    @error('lv_id')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <label for="type">Type</label>
                                    <input id="type" type="text"
                                        class="@error('type') is-invalid @enderror form-control" name="type"
                                        tabindex="1" value="{{ old('type') }}" required autofocus>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- endmodalTambah --}}
    </div>
@endsection
