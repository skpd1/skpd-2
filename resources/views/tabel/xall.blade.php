@extends('layouts.main')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data API</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Data</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-biasa">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>NMPER</th>
                                                <th>ANGGARAN</th>
                                                <th>REALISASI</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach ($response as $item)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $item->NMPER }}</td>
                                                    <td>{{ $item->ANGGARAN }}</td>
                                                    <td>{{ $item->REALISASI }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
