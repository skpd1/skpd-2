@extends('layouts.main')
@section('judul', 'Data Urusan')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Urusan</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-primary rounded-1" data-toggle="modal" data-target="#exampleModal">
                                    Import Excel
                                </button>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-urusan">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Kode Urusan</th>
                                                <th>Nama Urusan</th>
                                                <th>Nama Skpd</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/importurusan" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-body">
                            <div class="form-group">
                                <input type="file" name="file" required>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
