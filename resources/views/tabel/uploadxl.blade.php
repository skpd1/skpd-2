@extends('layouts.main')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data Upload</h1>
            </div>
            <div class="section-body">
                <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p>
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <div class="col-auto">
                                    <a href="/expupload" class="btn btn-danger">Export Excel</a>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-upload">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Unit Key</th>
                                                <th>Kode Unit</th>
                                                <th style="width:550px;">SKPD</th>

                                                <th>Id Urusan</th>
                                                <th>Id Program</th>
                                                <th>Kode Program</th>
                                                <th>Nama Program</th>

                                                <th>Id Kegiatan</th>
                                                <th>Number Kegiatan</th>
                                                <th>Nama Kegiatan</th>

                                                <th>Id Sub Kegiatan</th>
                                                <th>Kode Sub Kegiatan</th>
                                                <th>Nama Sub Kegiatan</th>

                                                <th>MTGKEY</th>
                                                <th>Kode Perekening</th>
                                                <th>Nama Perekening</th>
                                                <th>Anggaran</th>
                                                <th>Realisasi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
