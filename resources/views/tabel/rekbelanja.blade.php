@extends('layouts.main')
@section('judul', 'Data Rekening Belanja')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                @php
                    $user = auth()->user();
                    if ($user->skpd === null) {
                        echo '<h1>Kepala Daerah</h1>';
                    } else {
                        echo '<h1>' . $data_user->nm_skpd . '</h1>';
                    }
                @endphp
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            @can('admin')
                                <div class="card-header">
                                    <button class="btn btn-primary rounded-1" data-toggle="modal" data-target="#exampleModal">
                                        Import Excel
                                    </button>
                                </div>
                            @endcan

                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-biasa">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>MTGKEY</th>
                                                <th>Kode Rekening</th>
                                                <th>Nama Rekening</th>
                                                <th>Level </th>
                                                <th>Type</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach ($rekbelanjas as $row)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $row->mtg_key }}</td>
                                                    <td>{{ $row->kd_per }}</td>
                                                    <td>{{ $row->nm_per }}</td>
                                                    <td>{{ $row->kd_lv }}</td>
                                                    <td>{{ $row->type }}</td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <div class="modal fade" tabindex="-1" role="dialog" id="exampleModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Modal title</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <form action="/importrekbelanja" method="POST" enctype="multipart/form-data">
                        @csrf

                        <div class="modal-body">
                            <div class="form-group">
                                <input type="file" name="file" required>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Upload</button>
                        </div>
                </div>
                </form>
            </div>
        </div>
    </div>
@endsection
