@extends('layouts.main')
@section('judul', 'Dashboard')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                @can('admin')
                    <h1>Super Admin</h1>
                @elsecan('kepda')
                    <h1>Kepala Daerah</h1>
                @elsecan('user')
                    <h1> {{ $data->nm_skpd }}</h1>
                @endcan
            </div>
            <div class="row">
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Pendapatan</h4>
                        </div>
                        <div class="card-header">
                            <h4>Total Anggaran <span>@currency($pdptnanggaran)</span></h4>
                        </div>
                        <div class="card-body">
                            <canvas id="chartpendapatan"></canvas>
                        </div>
                        <div class="card-body">
                            <div class="card card-statistic-2 mb-0">
                                <div class="card-icon shadow-primary bg-danger">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Belum Realisasi</h4>
                                    </div>
                                    <h6>
                                        @currency($data_blm_rel_pdptn)
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card card-statistic-2 mb-0">
                                <div class="card-icon shadow-primary bg-primary">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Total Realisasi</h4>
                                    </div>
                                    <h6>
                                        @currency($pdptnrealisasi)
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- rekbelanja --}}
                <div class="col-12 col-md-6 col-lg-6">
                    <div class="card">
                        <div class="card-header">
                            <h4>Belanja</h4>
                        </div>
                        <div class="card-header">
                            <h4>Total Anggaran <span>@currency($blnjanggaran)</span> </h4>
                        </div>
                        <div class="card-body">
                            <canvas id="chartbelanja"></canvas>
                        </div>
                        <div class="card-body">
                            <div class="card card-statistic-2 mb-0">
                                <div class="card-icon shadow-primary bg-danger">
                                    <i class="fas fa-dollar-sign"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Belum Realisasi</h4>
                                    </div>
                                    <h6>
                                        @currency($data_blm_rel_blnj)
                                    </h6>
                                </div>
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="card card-statistic-2 mb-0">
                                <div class="card-icon shadow-primary bg-primary">
                                    <i class="fas fa-shopping-bag"></i>
                                </div>
                                <div class="card-wrap">
                                    <div class="card-header">
                                        <h4>Total Realisasi</h4>
                                    </div>
                                    <h6>
                                        @currency($blnjrealisasi)
                                    </h6>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-4">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-primary">
                            <i class="far fa-user"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Program</h4>
                            </div>
                            <div class="card-body">
                                {{ $jml_program }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-danger">
                            <i class="far fa-newspaper"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Kegiatan</h4>
                            </div>
                            <div class="card-body">
                                {{ $jml_kegiatan }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="card card-statistic-1">
                        <div class="card-icon bg-warning">
                            <i class="far fa-file"></i>
                        </div>
                        <div class="card-wrap">
                            <div class="card-header">
                                <h4>Sub Kegiatan</h4>
                            </div>
                            <div class="card-body">
                                {{ $jml_subkegiatan }}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
