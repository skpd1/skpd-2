@include('_partials.header')
@include('_partials.navbar')
@include('_partials.sidebar')
@yield('content')
@include('_partials.footer')
@include('_partials.script')
