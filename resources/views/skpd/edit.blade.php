{{-- {{ $user-id }} --}}
@extends('layouts.main')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Edit SKPD</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-8 mx-auto">
                        <div class="card">
                            <div class="card-header">
                                <h4>Edit Data SKPD </h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('dataskpd.update', $skpd->id) }}"
                                    class="needs-validation">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label for="unit_key">Unit Key</label>
                                        <input id="unit_key" type="text"
                                            class="@error('unit_key') is-invalid @enderror form-control" name="unit_key"
                                            tabindex="1" value="{{ old('unit_key', $skpd->unit_key) }}" required
                                            autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="kode_skpd">Kode SKPD</label>
                                        <input id="kode_skpd" type="text"
                                            class="@error('kode_skpd') is-invalid @enderror form-control" name="kode_skpd"
                                            tabindex="1" value="{{ old('kode_skpd', $skpd->kode_skpd) }}" required
                                            autofocus>
                                        @error('kode_skpd')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="nm_skpd">Nama SKPD</label>
                                        <input id="nm_skpd" type="text"
                                            class="@error('nm_skpd') is-invalid @enderror form-control" name="nm_skpd"
                                            tabindex="1" value="{{ old('nm_skpd', $skpd->nm_skpd) }}" required autofocus>
                                        @error('nm_skpd')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="lv_id">Id Level</label>
                                        <input id="lv_id" type="text"
                                            class="@error('lv_id') is-invalid @enderror form-control" name="lv_id"
                                            tabindex="1" value="{{ old('lv_id', $skpd->lv_id) }}" required autofocus>
                                        @error('lv_id')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <label for="type">Type</label>
                                        <input id="type" type="text"
                                            class="@error('type') is-invalid @enderror form-control" name="type"
                                            tabindex="1" value="{{ old('type', $skpd->type) }}" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                            Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
