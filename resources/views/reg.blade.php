@extends('layouts.auth')
@section('judul', 'Daftar Admin')
@section('content')
    <!-- Main Content -->
    <div id="app">
        <section class="section">
            <div class="container mt-5">
                <div class="row">
                    <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-6 offset-lg-3 col-xl-4 offset-xl-4">
                        <div class="login-brand">
                            <a href="#collapseExample" data-toggle="collapse">

                                <img src="../assets/img/stisla-fill.svg" alt="logo" width="100"
                                    class="shadow-light rounded-circle">
                            </a>
                        </div>
                        <div class="card card-danger">
                            <div class="card-header pt-0">
                                <h4 class="text-danger">Daftar Admin</h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="/regadmin" novalidate="">
                                    @csrf
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input id="username" type="text"
                                            class="form-control @error('username') is-invalid @enderror" name="username"
                                            tabindex="1" required autofocus value="{{ old('username') }}">
                                        @error('username')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>

                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password</label>
                                        </div>
                                        <input id="password" type="password"
                                            class="form-control @error('password') is-invalid @enderror" name="password"
                                            tabindex="2" required>
                                        @error('password')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-danger btn-lg btn-block" tabindex="4">
                                            Registrasi Akun
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div class="simple-footer">
                            Copyright &copy; 2018 <div class="bullet"></div> Simulasi Data <a href="https://google.id/"
                                class="text-danger">
                                Pemko Padang
                                <a href="/login" class="text-danger">
                                    Sign In
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
