@extends('layouts.main')
@section('judul', 'Data User')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Data User</h1>
            </div>

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div><br />
            @endif
            <div class="section-body">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <button class="btn btn-primary rounded" data-toggle="modal" data-target="#ModalTambah">
                                    <i class="fas fa-plus mr-2"></i>
                                    Tambah User SKPD </button>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped table-biasa">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Nama Kadis</th>
                                                <th>NIP</th>
                                                <th>SKPD</th>
                                                <th>Username</th>
                                                <th>Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php $no = 1; @endphp
                                            @foreach ($users as $row)
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $row->name }}</td>
                                                    <td>{{ $row->nip }}</td>
                                                    <td>{{ $row->nm_skpd }}</td>
                                                    <td>{{ $row->username }}</td>
                                                    <td style="width: 350px">
                                                        <div class="d-inline-block">
                                                            <a href="#" data-target="#ModalLihat{{ $row->id }}"
                                                                class="btn btn-info" data-toggle="modal"> <i
                                                                    class="fas fa-eye"></i> Lihat</a>
                                                            <a href="{{ route('datauser.edit', $row->id) }}"
                                                                class="btn btn-warning"> <i class="fas fa-eye"></i>
                                                                Edit</a>
                                                            <button class="btn btn-danger" data-toggle="modal"
                                                                data-target="#ModalHapus">
                                                                <i class="fas fa-trash"></i>
                                                                Hapus </button>
                                                        </div>

                                                    </td>
                                                </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        {{-- modalTambah --}}
        <div class="modal fade" role="dialog" id="ModalTambah">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Tambah User SKPD</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="card-body">
                            <form method="POST" action="/datauser" class="needs-validation">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="name">Nama Kepala Dinas</label>
                                    <input id="name" type="text"
                                        class="@error('name') is-invalid @enderror form-control" name="name"
                                        tabindex="1" value="{{ old('name') }}" required autofocus>
                                </div>
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input id="username" type="text"
                                        class="@error('username') is-invalid @enderror form-control" name="username"
                                        tabindex="1" value="{{ old('username') }}" required autofocus>
                                    @error('username')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                                <div class="form-group">
                                    <div class="d-block">
                                        <label for="password" class="control-label">Password</label>
                                    </div>
                                    <input id="password" type="password"
                                        class="@error('password') is-invalid @enderror form-control" name="password"
                                        tabindex="2" required>
                                    <div class="invalid-feedback">
                                        please fill in your password
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="skpd">Nama SKPD</label>
                                    <select class="form-control select2" id="skpd" name="skpd">
                                        @foreach ($skpds as $row)
                                            <option value="{{ $row->id }}">{{ $row->nm_skpd }}</option>
                                        @endforeach

                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="nip">NIP</label>
                                    <input id="nip" type="number"
                                        class="@error('nip') is-invalid @enderror form-control" name="nip"
                                        tabindex="1" value="{{ old('nip') }}" required autofocus>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Simpan
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- endmodalTambah --}}

        {{-- modalLihat --}}
        @foreach ($users as $row)
            <div class="modal fade" tabindex="-1" role="dialog" id="ModalLihat{{ $row->id }}">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title">Data Detil</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="row">
                            <div class="col-md-6 ml-4">
                                <address>
                                    <strong>Nama :</strong><br>
                                    {{ $row->name }}<br>
                                </address>
                                <address>
                                    <strong>NIP :</strong><br>
                                    {{ $row->nip }}<br>
                                </address>
                                <address>
                                    <strong>SKPD :</strong><br>
                                    {{ $row->nm_skpd }}<br>
                                </address>
                            </div>
                        </div>
                        <div class="modal-footer bg-whitesmoke br">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
        {{-- endmodalLihat --}}
    </div>
@endsection
