  <!-- General JS Scripts -->
  <script src="https://code.jquery.com/jquery-3.3.1.min.js"
      integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
      integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
      integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
  <script src="{{ asset('assets/js/stisla.js') }}"></script>

  <!-- JS Libraies -->
  <script src="{{ asset('node_modules/datatables/media/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('node_modules/datatables.net-bs4/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('node_modules/datatables.net-select-bs4/js/select.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('node_modules/prismjs/prism.js') }}"></script>
  <script src="{{ asset('node_modules/select2/dist/js/select2.full.min.js') }}"></script>

  <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
  <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>

  <!-- Page Specific JS File -->
  <script src="{{ asset('assets/js/scripts.js') }}"></script>
  <script src="{{ asset('assets/js/custom.js') }}"></script>
  <script>
      $(".table-biasa").dataTable({
          aLengthMenu: [
              [25, 50, 100, 200, -1],
              [25, 50, 100, 200, 'All']
          ],
          ordering: false,
          columnDefs: [{
              sortable: false,
              // "targets": 'All'
          }]
      });
  </script>
  @if (Request::segment(1) === 'dataurusan')
      <script>
          $("#table-urusan").dataTable({
              aLengthMenu: [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
              ],
              processing: true,
              serverSide: true,
              ordering: true,
              // searchable: true,
              order: [
                  [0, 'asc']
              ],
              columnDefs: [{
                  sortable: false,
                  targets: 'All'
              }],
              ajax: '{!! route('dataurusan.index') !!}',
              columns: [
                  // 
                  {
                      data: null,
                      class: "align-top",
                      orderable: false,
                      searchable: false,
                      render: function(data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  {
                      data: 'kode_urusan',
                      name: 'urusans.kode_urusan',
                  },
                  {
                      data: 'nm_urusan',
                      name: 'urusans.nm_urusan',
                  },
                  {
                      data: 'nm_skpd',
                      name: 'skpds.nm_skpd',
                  },
                  // {
                  //     data: 'nm_skpd',
                  //     name: 'urusans.nm_skpd',
                  // },
              ],
          });
      </script>
  @elseif (Request::segment(1) === 'dataprogram')
      <script>
          $("#table-program").dataTable({
              aLengthMenu: [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
              ],
              processing: true,
              serverSide: true,
              ordering: false,
              searchable: true,
              order: [
                  [0, 'asc']
              ],
              columnDefs: [{
                  sortable: false,
                  targets: 'All'
              }],
              ajax: '{!! route('dataprogram.index') !!}',
              columns: [
                  // 
                  {
                      data: null,
                      class: "align-top",
                      orderable: false,
                      searchable: false,
                      render: function(data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  {
                      data: 'id_program',
                      name: 'programs.id_program',
                  },
                  {
                      data: 'unit_key',
                      name: 'programs.unit_key',
                  },
                  {
                      data: 'nm_program',
                      name: 'programs.nm_program',
                  },
                  {
                      data: 'nu_program',
                      name: 'programs.nu_program',
                  },
              ],
          });
      </script>
  @elseif (Request::segment(1) === 'dataprogramskpd')
      <script>
          $("#table-programskpd").dataTable({
              aLengthMenu: [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
              ],
              processing: true,
              serverSide: true,
              ordering: true,
              searchable: true,
              order: [
                  [0, 'asc']
              ],
              columnDefs: [{
                  sortable: false,
                  targets: 'All'
              }],
              ajax: '{!! route('dataprogramskpd.index') !!}',
              columns: [
                  // 
                  {
                      data: null,
                      class: "align-top",
                      orderable: false,
                      searchable: false,
                      render: function(data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  {
                      data: 'nm_skpd',
                      name: 'skpds.nm_skpd',
                  },
                  {
                      data: 'id_program',
                      name: 'programskpds.id_program',
                  },
                  {
                      data: 'nm_program',
                      name: 'programskpds.nm_program',
                  },
              ],
          });
      </script>
  @elseif (Request::segment(1) === 'datakegiatan')
      <script>
          $("#table-kegiatan").dataTable({
              aLengthMenu: [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
              ],
              processing: true,
              serverSide: true,
              ordering: false,
              searchable: true,
              order: [
                  [0, 'asc']
              ],
              columnDefs: [{
                  sortable: false,
                  targets: 'All'
              }],
              ajax: '{!! route('datakegiatan.index') !!}',
              columns: [
                  // 
                  {
                      data: null,
                      class: "align-top",
                      orderable: false,
                      searchable: false,
                      render: function(data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  {
                      data: 'id_kegiatan',
                      name: 'kegiatans.id_kegiatan',
                  },
                  {
                      data: 'nm_kegiatan',
                      name: 'kegiatans.nm_kegiatan',
                  },
                  {
                      data: 'nu_kegiatan',
                      name: 'kegiatans.nu_kegiatan',
                  },
              ],
          });
      </script>
  @elseif (Request::segment(1) === 'datasubkegiatan')
      <script>
          $("#table-subkegiatan").dataTable({
              aLengthMenu: [
                  [25, 50, 100, 200, -1],
                  [25, 50, 100, 200, "All"]
              ],
              processing: true,
              serverSide: true,
              ordering: true,
              searchable: true,
              order: [
                  [0, 'asc']
              ],
              columnDefs: [{
                  sortable: false,
                  targets: 'All'
              }],
              ajax: '{!! route('datasubkegiatan.index') !!}',
              columns: [
                  // 
                  {
                      data: null,
                      class: "align-top",
                      orderable: false,
                      searchable: false,
                      render: function(data, type, row, meta) {
                          return meta.row + meta.settings._iDisplayStart + 1;
                      }
                  },
                  {
                      data: 'unit_key',
                      name: 'subkegiatans.unit_key',
                  },
                  {
                      data: 'kode_subkeg',
                      name: 'subkegiatans.kode_subkeg',
                  },
                  {
                      data: 'nm_subkeg',
                      name: 'subkegiatans.nm_subkeg',
                  },
                  {
                      data: 'nm_skpd',
                      name: 'skpds.nm_skpd',
                  },
              ],
          });
      </script>
  @elseif (Request::segment(1) === 'perekening')
      <script>
          $(document).ready(function() {

              function numberWithCommas(x) {
                  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
              }
              $("#table-perekening").dataTable({

                  dom: 'lBrtip<"actions">',
                  buttons: ['print'],

                  aLengthMenu: [
                      [5, 10, 25, 50, 100, 200, -1],
                      [5, 10, 25, 50, 100, 200, "All"]
                  ],
                  processing: true,
                  serverSide: true,
                  ordering: false,
                  searchable: false,
                  order: [
                      [0, 'asc']
                  ],
                  columnDefs: [{
                      //   sortable: false,
                      //   targets: 'All',
                      targets: [7, 8, 9],
                      render: $.fn.dataTable.render.number('.')

                  }],
                  ajax: '{!! route('perekening.index') !!}',
                  columns: [
                      // 
                      {
                          data: null,
                          class: "align-top",
                          orderable: false,
                          searchable: false,
                          render: function(data, type, row, meta) {
                              return meta.row + meta.settings._iDisplayStart + 1;
                          }
                      },
                      {
                          data: 'nm_skpd',
                          name: 'skpds.nm_skpd',
                      },
                      {
                          data: 'nm_program',
                          name: 'programs.nm_program',
                      },
                      {
                          data: 'nm_kegiatan',
                          name: 'kegiatans.nm_kegiatan',
                      },
                      {
                          data: 'nm_subkeg',
                          name: 'subkegiatans.nm_subkeg',
                      },
                      {
                          data: 'kd_per',
                          name: 'belanjas.kd_per',
                      },
                      {
                          data: 'nm_per',
                          name: 'belanjas.nm_per',
                      },
                      {
                          data: 'anggaran',
                          name: 'perekenings.anggaran',
                      },
                      {
                          data: 'realisasi',
                          name: 'perekenings.realisasi',
                      },
                      {
                          data: 'sisa',
                          custom: 'sisa',
                          orderable: false,
                          searchable: false
                      },
                  ],
                  initComplete: function() {
                      this.api().columns([1, 2, 3, 4, 5]).every(function() {
                          var column = this;
                          var input = document.createElement("input");
                          $(input).appendTo($(column.header()).empty())
                              .on('change', function() {
                                  column.search($(this).val(), false, false, true).draw();
                              });
                      });
                  },


                  footerCallback: function(row, data) {
                      var api = this.api(),
                          data;
                      var intVal = function(i) {
                          return typeof i === 'string' ?
                              i.replace(/[\$,]/g, '') * 1 :
                              typeof i === 'number' ?
                              i : 0;
                      };
                      pAnggaran = api.column(7, {
                              page: 'current'
                          })
                          .data().reduce(function(a, b) {
                              return intVal(a) + intVal(b);
                          }, 0)
                      $(api.column(7).footer()).html(
                          numberWithCommas(pAnggaran)
                      )
                      pRealisasi = api.column(8, {
                              page: 'current'
                          })
                          .data().reduce(function(a, b) {
                              return intVal(a) + intVal(b);
                          }, 0)
                      $(api.column(8).footer()).html(
                          numberWithCommas(pRealisasi)
                      )
                      pSisa = api.column(9, {
                              page: 'current'
                          })
                          .data().reduce(function(a, b) {
                              return intVal(a) + intVal(b);
                          }, 0)
                      $(api.column(9).footer()).html(
                          numberWithCommas(pSisa)
                      )
                  }
              });
          });
      </script>
  @elseif (Request::segment(1) === 'relpendapatan')
      @can('admin', 'kepda')
          <script>
              $(document).ready(function() {

                  function numberWithCommas(x) {
                      return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.');
                  }

                  $("#table-relpendapatan").dataTable({

                      dom: 'lBrtip<"actions">',
                      buttons: ['print'],

                      aLengthMenu: [
                          [5, 10, 25, 50, 100, 200, -1],
                          [5, 10, 25, 50, 100, 200, "All"]
                      ],
                      processing: true,
                      serverSide: true,
                      ordering: false,
                      //   searchable: true,
                      order: [
                          [0, 'asc']
                      ],
                      columnDefs: [
                          //
                          {
                              //   sortable: false,
                              //   targets: 'All',
                              targets: [4, 5],
                              render: $.fn.dataTable.render.number('.')
                          },
                          {
                              targets: [6],
                              render: $.fn.dataTable.render.number(',', '.', 2, '', '%')
                          }
                      ],
                      ajax: '{!! route('relpendapatan.index') !!}',
                      columns: [
                          // 
                          {
                              data: null,
                              class: "align-top",
                              orderable: false,
                              searchable: false,
                              render: function(data, type, row, meta) {
                                  return meta.row + meta.settings._iDisplayStart + 1;
                              }
                          },
                          {
                              data: 'nm_skpd',
                              name: 'skpds.nm_skpd',
                          },
                          {
                              data: 'kd_per',
                              name: 'pendapatans.kd_per',
                          },
                          {
                              data: 'nm_per',
                              name: 'pendapatans.nm_per',
                          },
                          {
                              data: 'anggaran',
                              name: 'relpendapatans.anggaran',
                              orderable: false,
                              searchable: false
                          },
                          {
                              data: 'realisasi',
                              name: 'relpendapatans.realisasi',
                              orderable: false,
                              searchable: false
                          },
                          {
                              data: 'percent',
                              custom: 'percent',
                              orderable: false,
                              searchable: false
                          },

                      ],

                      initComplete: function() {
                          this.api().columns([1, 2, 3]).every(function() {
                              var column = this;
                              var input = document.createElement("input");
                              $(input).appendTo($(column.header()).empty())
                                  .on('change', function() {
                                      column.search($(this).val(), false, false, true).draw();
                                  });
                          });
                      },

                      footerCallback: function(row, data) {
                          var api = this.api(),
                              data;
                          var intVal = function(i) {
                              return typeof i === 'string' ?
                                  i.replace(/[\$,]/g, '') * 1 :
                                  typeof i === 'number' ?
                                  i : 0;
                          };
                          pAnggaran = api.column(4, {
                                  page: 'current'
                              })
                              .data().reduce(function(a, b) {
                                  return intVal(a) + intVal(b);
                              }, 0)
                          $(api.column(4).footer()).html(
                              numberWithCommas(pAnggaran)
                          )
                          pRealisasi = api.column(5, {
                                  page: 'current'
                              })
                              .data().reduce(function(a, b) {
                                  return intVal(a) + intVal(b);
                              }, 0)
                          $(api.column(5).footer()).html(
                              numberWithCommas(pRealisasi)
                          )
                          pPercent = api.column(6, {
                                  page: 'current'
                              })
                              .data().reduce(function(a, b) {
                                  return intVal(a) + intVal(b);
                              }, 0)
                          $(api.column(6).footer()).html(
                              '-'
                          )
                      }
                  });
              });
          </script>
          {{-- @elsecan('kepda') --}}
      @endcan
  @elseif (Request::segment(1) === 'dashboard')
      <script src="{{ asset('node_modules/chart.js/dist/Chart.min.js') }}"></script>
      <script>
          var pdptn = document.getElementById("chartpendapatan").getContext('2d');
          var myChart = new Chart(pdptn, {
              type: 'pie',
              data: {
                  datasets: [{
                      data: [
                          '<?= $chartdata_x ?>',
                          '<?= $chartdata ?>',
                      ],
                      backgroundColor: [
                          '#fc544b',
                          '#6777ef',
                      ],
                      label: 'Dataset 1'
                  }],
                  labels: [
                      'Belum Realisasi',
                      'Telah Realisasi'
                  ],
              },
              options: {
                  responsive: true,
                  legend: {
                      position: 'bottom',
                  },
              }
          });

          var blnj = document.getElementById("chartbelanja").getContext('2d');
          var myChart = new Chart(blnj, {
              type: 'pie',
              data: {
                  datasets: [{
                      data: [
                          '<?= $chartbelanja_blm ?>',
                          '<?= $chartbelanja_tlh ?>',
                      ],
                      backgroundColor: [
                          '#fc544b',
                          '#6777ef',
                      ],
                      label: 'Dataset 2'
                  }],
                  labels: [
                      'Belum Realisasi',
                      'Telah Realisasi'
                  ],
              },
              options: {
                  responsive: true,
                  legend: {
                      position: 'bottom',
                  },
              }
          });


          // pdptn_admin
          var pdptn_admin = document.getElementById("chartpendapatan_admin").getContext('2d');
          var myChart = new Chart(pdptn_admin, {
              type: 'pie',
              data: {
                  datasets: [{
                      data: [
                          '<?= $chart_relpendapatans_blm_admin ?>',
                          '<?= $chart_relpendapatans_tlh_admin ?>',
                      ],
                      backgroundColor: [
                          '#fc544b',
                          '#6777ef',
                      ],
                      label: 'Dataset 1'
                  }],
                  labels: [
                      'Belum Realisasi',
                      'Telah Realisasi'
                  ],
              },
              options: {
                  responsive: true,
                  legend: {
                      position: 'bottom',
                  },
              }
          });

          // blnj_admin
          var blnja_admin = document.getElementById("chartbelanja_admin").getContext('2d');
          var myChart = new Chart(blnja_admin, {
              type: 'pie',
              data: {
                  datasets: [{
                      data: [
                          '<?= $chart_perekenings_blm_admin ?>',
                          '<?= $chart_perekenings_tlh_admin ?>',
                          //   10,
                          //   90
                      ],
                      backgroundColor: [
                          '#fc544b',
                          '#6777ef',
                      ],
                      label: 'Dataset 1'
                  }],
                  labels: [
                      'Belum Realisasi',
                      'Telah Realisasi'
                  ],
              },
              options: {
                  responsive: true,
                  legend: {
                      position: 'bottom',
                  },
              }
          });
      </script>
  @endif
  </body>

  </html>
