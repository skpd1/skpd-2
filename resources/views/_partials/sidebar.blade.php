<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            @can('admin')
                <a href="#">Admin</a>
            @elsecan('kepda')
                <a href="#">Kepala Daerah</a>
            @elsecan('user')
                <a href="#">User</a>
            @endcan
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="#">St</a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">Dashboard</li>
            <li class="{{ Request::segment(1) === 'dashboard' ? 'active' : '' }} ">
                <a class="nav-link" href="/dashboard"><i class="fas fa-pencil-ruler"></i>
                    <span>Dashboard</span></a>
            </li>
            @can('admin')
                <li class="{{ Request::segment(1) === 'dataskpd' ? 'active' : '' }} ">
                    <a class="nav-link" href="/dataskpd"><i class="fas fa-pencil-ruler"></i>
                        <span>SKPD</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'dataurusan' ? 'active' : '' }} ">
                    <a class="nav-link" href="/dataurusan"><i class="fas fa-pencil-ruler"></i>
                        <span>Urusan</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'dataprogram' ? 'active' : '' }} ">
                    <a class="nav-link" href="/dataprogram"><i class="fas fa-pencil-ruler"></i>
                        <span>Program</span></a>
                </li>
                {{-- <li class="{{ Request::segment(1) === 'dataprogramskpd' ? 'active' : '' }} ">
                    <a class="nav-link" href="/dataprogramskpd"><i class="fas fa-pencil-ruler"></i>
                        <span>Program SKPD</span></a>
                </li> --}}
                <li class="{{ Request::segment(1) === 'datakegiatan' ? 'active' : '' }} ">
                    <a class="nav-link" href="/datakegiatan"><i class="fas fa-pencil-ruler"></i>
                        <span>Kegiatan</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'datasubkegiatan' ? 'active' : '' }} ">
                    <a class="nav-link" href="/datasubkegiatan"><i class="fas fa-pencil-ruler"></i>
                        <span>Sub Kegiatan</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'rekbelanja' ? 'active' : '' }} ">
                    <a class="nav-link" href="/rekbelanja"><i class="fas fa-pencil-ruler"></i>
                        <span>Rekening Belanja</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'rekpendapatan' ? 'active' : '' }} ">
                    <a class="nav-link" href="/rekpendapatan"><i class="fas fa-pencil-ruler"></i>
                        <span>Rekening Pendapatan</span></a>
                </li>
                <li class="{{ Request::segment(1) === 'datauser' ? 'active' : '' }} ">
                    <a class="nav-link" href="/datauser"><i class="fas fa-pencil-ruler"></i>
                        <span>Data User</span></a>
                </li>

                {{-- <li class="{{ Request::segment(1) === 'dataupload' ? 'active' : '' }} ">
                    <a class="nav-link" href="/dataupload"><i class="fas fa-pencil-ruler"></i>
                        <span>Export Dari URL</span></a>
                </li> --}}
                {{-- <li class="{{ Request::segment(1) === 'adduser' ? 'active' : '' }} ">
                    <a class="nav-link" href="/adduser"><i class="fas fa-pencil-ruler"></i>
                        <span>User</span></a>
                </li> --}}
                {{-- <li class="{{ Request::segment(1) === 'addurusan' ? 'active' : '' }} ">
                    <a class="nav-link" href="/addurusan"><i class="fas fa-pencil-ruler"></i>
                        <span>Tambah Urusan</span></a>
                </li> --}}
                {{-- <li class="{{ Request::segment(1) === 'addskpd' ? 'active' : '' }} ">
                    <a class="nav-link" href="/addskpd"><i class="fas fa-pencil-ruler"></i>
                        <span>Tambah SKPD</span></a>
                </li> --}}
            @elsecan('kepda')
                <li class="{{ Request::segment(1) === 'datauser' ? 'active' : '' }} ">
                    <a class="nav-link" href="/datauser"><i class="fas fa-pencil-ruler"></i>
                        <span>Data User</span></a>
                </li>
            @endcan

            <li class="{{ Request::segment(1) === 'relpendapatan' ? 'active' : '' }} ">
                <a class="nav-link" href="/relpendapatan"><i class="fas fa-pencil-ruler"></i>
                    <span>Realisasi Pendapatan</span></a>
            </li>
            <li class="{{ Request::segment(1) === 'perekening' ? 'active' : '' }} ">
                <a class="nav-link" href="/perekening"><i class="fas fa-pencil-ruler"></i>
                    <span>Realisasi Belanja</span></a>
            </li>
        </ul>
    </aside>
</div>
