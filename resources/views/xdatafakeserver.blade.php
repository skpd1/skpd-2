@extends('layouts.main')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>DataTables</h1>
            </div>

            <div class="section-body">
                <h2 class="section-title">Example Title</h2>
                <p class="section-lead">
                    Lorem ipsum dolor sit, amet consectetur adipisicing elit. Beatae, ipsa.
                </p>

                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4>Data</h4>
                            </div>
                            <div class="card-body">
                                <div class="table-responsive">
                                    <table class="table table-striped" id="table-serverside">
                                        <thead>
                                            <tr>
                                                <th>No</th>
                                                <th>Unitkey</th>
                                                <th>Kdunit</th>
                                                <th>Skpd</th>
                                                <th>Id_urusan</th>
                                                <th>Id_program</th>
                                                <th>Kd_program</th>
                                                <th>Nm_program</th>
                                                <th>Id_keg</th>
                                                <th>Nu_keg</th>
                                                <th>Nm_keg</th>
                                                <th>Id_subkeg</th>
                                                <th>Kd_subkeg</th>
                                                <th>Nm_subkeg</th>
                                                <th>Mtgkey</th>
                                                <th>Kd_per</th>
                                                <th>Nm_per</th>
                                                <th>Anggaran</th>
                                                <th>Realisasi</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
