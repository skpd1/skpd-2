@extends('layouts.main')
@section('content')
    <!-- Main Content -->
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>Edit User</h1>
            </div>
            <div class="section-body">
                <div class="row">
                    <div class="col-8 mx-auto">
                        <div class="card">
                            <div class="card-header">
                                <h4>Edit Data </h4>
                            </div>
                            <div class="card-body">
                                <form method="POST" action="{{ route('datauser.update', $user->id) }}"
                                    class="needs-validation">
                                    @csrf
                                    @method('PATCH')
                                    <div class="form-group">
                                        <label for="name">Nama Kepala Dinas</label>
                                        <input id="name" type="text"
                                            class="@error('name') is-invalid @enderror form-control" name="name"
                                            tabindex="1" value="{{ $user->name }}" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <label for="username">Username</label>
                                        <input id="username" type="text"
                                            class="@error('username') is-invalid @enderror form-control" name="username"
                                            tabindex="1" value="{{ $user->username }}" required autofocus>
                                        @error('username')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <div class="form-group">
                                        <div class="d-block">
                                            <label for="password" class="control-label">Password</label>
                                        </div>
                                        <input id="password" type="password"
                                            class="@error('password') is-invalid @enderror form-control" name="password"
                                            tabindex="2" required>
                                        <div class="invalid-feedback">
                                            please fill in your password
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="skpd">Nama SKPD</label>
                                        <select class="form-control select2" id="skpd" name="skpd_id" required>
                                            <option value="">Pilih Data SKPD</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label for="nip">NIP</label>
                                        <input id="nip" type="number"
                                            class="@error('nip') is-invalid @enderror form-control" name="nip"
                                            tabindex="1" value="{{ $user->nip }}" required autofocus>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                            Simpan
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
