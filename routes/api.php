<?php

use App\Http\Controllers\DataController;
use App\Http\Controllers\FakerController;
use App\Http\Controllers\SkpdController;
// use App\Http\Controllers\GetdataController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/



Route::get('/data', [DataController::class, 'index']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
// Route::get('/getdata', [GetdataController::class, 'index']);
