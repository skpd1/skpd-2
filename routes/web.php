<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\SkpdController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ChartController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\UrusanController;
use App\Http\Controllers\BelanjaController;
use App\Http\Controllers\ProgramController;
use App\Http\Controllers\KegiatanController;
use App\Http\Controllers\RegisterController;
use App\Http\Controllers\DashboardController;

use App\Http\Controllers\PendapatanController;
use App\Http\Controllers\PerekeningController;
use App\Http\Controllers\ProgramskpdController;
use App\Http\Controllers\SubkegiatanController;
use App\Http\Controllers\RelpendapatanController;

Route::get('/login', [LoginController::class, 'index'])->name('login')->middleware('guest');
Route::post('/auth', [LoginController::class, 'auth'])->name('auth')->middleware('guest');


Route::post('/logout', [LoginController::class, 'logout'])->middleware('auth');


Route::get('/regadmin', [RegisterController::class, 'index'])->middleware('guest');
Route::post('/regadmin', [RegisterController::class, 'store']);

Route::get('/dashboard', [DashboardController::class, 'index'])->middleware('auth');
Route::get('/data-json', [DashboardController::class, 'datajson'])->middleware('guest');
Route::get('/get-json', [DashboardController::class, 'getjson'])->middleware('guest');


Route::resource('/datauser', UserController::class)->middleware('admin');

Route::resource('/dataskpd', SkpdController::class)->middleware('admin');
Route::post('/importskpd', [SkpdController::class, 'importexcel'])->name('importexcel')->middleware('admin'); //tambah class skpd

Route::resource('/dataprogram', ProgramController::class)->middleware('admin');
Route::post('/importprogram', [ProgramController::class, 'importexcel'])->name('program.importexcel')->middleware('admin');

Route::resource('/dataprogramskpd', ProgramskpdController::class)->middleware('admin');
Route::post('/importprogramskpd', [ProgramskpdController::class, 'importexcel'])->name('programskpd.importexcel')->middleware('admin');

Route::resource('/datakegiatan', KegiatanController::class)->middleware('admin');
Route::post('/importkegiatan', [KegiatanController::class, 'importexcel'])->name('kegiatan.importexcel')->middleware('admin');

Route::resource('/datasubkegiatan', SubkegiatanController::class)->middleware('admin');
Route::post('/importsubkegiatan', [SubkegiatanController::class, 'importexcel'])->name('subkegiatan.importexcel')->middleware('admin');

Route::resource('/rekbelanja', BelanjaController::class)->middleware('admin');
Route::post('/importrekbelanja', [BelanjaController::class, 'importexcel'])->name('rekbelanja.importexcel')->middleware('admin');

Route::resource('/rekpendapatan', PendapatanController::class)->middleware('admin');
Route::post('/importrekpendapatan', [PendapatanController::class, 'importexcel'])->name('rekpendapatan.importexcel')->middleware('admin');

Route::resource('/dataurusan', UrusanController::class)->middleware('admin');
Route::post('/importurusan', [UrusanController::class, 'importexcel'])->name('urusan.importexcel')->middleware('admin');

Route::resource('/perekening', PerekeningController::class)->middleware('auth');
Route::post('/importperekening', [PerekeningController::class, 'importexcel'])->name('perekening.importexcel')->middleware('admin');

Route::resource('/relpendapatan', RelpendapatanController::class)->middleware('auth');
// Route::get('/relpendapatanxxx', [RelpendapatanController::class, 'xxx'])->name('relpendapatan.index_admin')->middleware('auth');
Route::post('/importrelpendapatan', [RelpendapatanController::class, 'importexcel'])->name('relpendapatan.importexcel')->middleware('admin');
// Route::get('/relpendapatan', RelpendapatanController::class)->middleware('auth');

Route::get('/total-anggaran', [PerekeningController::class, 'totalAnggaran']);


Route::get('/chart', [ChartController::class, 'index']);


// Route::get('/ajax', 'UserController@ajax');
// Route::post('/datauser', [UserController::class, 'ajax'])->name('datauser.ajax');
// Route::get('/datauser', [UserController::class, 'add']);
