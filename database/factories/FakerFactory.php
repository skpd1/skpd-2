<?php

namespace Database\Factories;

use Illuminate\Support\Carbon;
use Illuminate\Database\Eloquent\Factories\Factory;

class FakerFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'unitkey' => '345_',
            'kdunit' => $this->faker->sentence(mt_rand(2, 3)),
            'skpd' => $this->faker->sentence(mt_rand(2, 3)),
            'id_urusan' => $this->faker->sentence(mt_rand(2, 3)),
            'id_program' => $this->faker->sentence(mt_rand(2, 3)),
            'kd_program' => '02.',
            'nm_program' => $this->faker->sentence(mt_rand(2, 3)),
            'id_keg' => '1412_',
            'nu_keg' => '2.03.',
            'nm_keg' => $this->faker->sentence(mt_rand(2, 3)),
            'id_subkeg' => $this->faker->sentence(mt_rand(2, 3)),
            'kd_subkeg' => $this->faker->sentence(mt_rand(2, 3)),
            'nm_subkeg' => $this->faker->sentence(mt_rand(2, 3)),
            'mtgkey' => '5225_',
            'kd_per' => $this->faker->sentence(mt_rand(2, 3)),
            'nm_per' => $this->faker->sentence(mt_rand(2, 3)),
            'anggaran' => $this->faker->sentence(mt_rand(2, 3)),
            'realisasi' => $this->faker->sentence(mt_rand(2, 3)),
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ];
    }
}
