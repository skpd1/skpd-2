<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ProgramSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('programs')->insert([
            'kode_program' => '311_',
            'nm_program' => 'Program Pemberdayaan Usaha Menengah, Usaha Kecil, Dan Usaha Mikro (Umkm)',
            'unit_key' => '345_',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('programs')->insert([
            'kode_program' => '422_',
            'nm_program' => 'Program Penunjang Urusan Pemerintahan Daerah Kabupaten/Kota',
            'unit_key' => '345_',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
