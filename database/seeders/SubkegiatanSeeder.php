<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SubkegiatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('subkegiatans')->insert([
            'kode_subkeg' => '22.01.02.',
            'nm_subkeg' => 'Koordinasi dan Sinkronisasi Pembinaan Pelaksanaan Penanaman Modal',
            'unit_key' => '6573_',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('subkegiatans')->insert([
            'kode_subkeg' => '22.01.02.',
            'nm_subkeg' => 'Pemantauan Pemenuhan Komitmen Perizinan dan Non Perizinan Penanaman Modal',
            'unit_key' => '6566_',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
