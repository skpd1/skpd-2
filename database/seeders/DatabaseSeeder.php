<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $this->call([
            KadisSeeder::class,
            UserSeeder::class,
            // SkpdSeeder::class,
            // UrusanSeeder::class,
            // ProgramSeeder::class,
            // KegiatanSeeder::class,
            // SubkegiatanSeeder::class,
            // PerekeningSeeder::class,
            // UploadxlSeeder::class,
            // DataSeeder::class,
        ]);
    }
}
