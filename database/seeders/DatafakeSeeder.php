<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Carbon\Carbon;
use Faker\Factory as Faker;

class DatafakeSeeder extends Seeder
{
	public function run()
	{
		$faker = Faker::create('id_ID');
		$count = 50;
		for ($i = 1; $i <= $count; $i++) {
			\DB::table('datafake')->insert([
				'nama' => $faker->name,
				'email' => $faker->safeEmail,
				'alamat' => $faker->address,
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
			]);
		}
	}
}
