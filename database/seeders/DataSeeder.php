<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('data')->insert([
            'skpd_id' => 1,
            'urusan_id' => 1,
            'program_id' => 1,
            'kegiatan_id' => 1,
            'subkegiatan_id' => 1,
            'nm_mtgkey' => '5139_',
            'kode_per' => '5.1.02.01.01.0025.',
            'nm_per' => 'Belanja Alat/Bahan untuk Kegiatan Kantor- Kertas dan Cover',
            'anggaran' => '7009400.00',
            'realisasi' => '5510000.00',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}


//         ['unitkey'] => 345_
//         ['kdunit'] => 1.01.0.00.0.00.01.
//         ['skpd'] => Dinas Pendidikan Dan Kebudayaan
//         ['idurusan'] => 55_
//         ['idprgrm'] => 203_
//         ['kdprgrm'] => 02.
//         ['nmprgrm'] => Program Pengelolaan Pendidikan
//         ['idkeg'] => 1412_
//         ['nukeg'] => 2.03.
//         ['nmkeg'] => Pengelolaan Pendidikan Anak Usia Dini (Paud)
//         ['idsubkeg'] => 4380_
//         ['kdsubkeg'] => 2.03.15.
//         ['nmsubkeg'] => Penyediaan Pendidik Dan Tenaga Kependidikan Bagi Satuan Paud
//         ['mtgkey'] => 5225_
//         ['kdper'] => 5.1.02.02.01.0013.
//         ['nmper'] => Belanja Jasa Tenaga Pendidikan
//         ['anggaran'] => 2226000000.00
//         ['realisasi'] => 2226000000.00

            // [UNITKEY] => 345_
            // [KDUNIT] => 1.01.0.00.0.00.01.
            // [SKPD] => DINAS PENDIDIKAN DAN KEBUDAYAAN
            // [IDURUSAN] => 55_
            // [IDPRGRM] => 203_
            // [KDPRGRM] => 02.
            // [NMPRGRM] => PROGRAM PENGELOLAAN PENDIDIKAN
            // [IDKEG] => 1412_
            // [NUKEG] => 2.03.
            // [NMKEG] => Pengelolaan Pendidikan Anak Usia Dini (PAUD)
            // [IDSUBKEG] => 4380_
            // [KDSUBKEG] => 2.03.15.
            // [NMSUBKEG] => Penyediaan Pendidik dan Tenaga Kependidikan bagi Satuan PAUD
            // [mtgkey] => 5225_
            // [KDPER] => 5.1.02.02.01.0013.
            // [NMPER] => Belanja Jasa Tenaga Pendidikan
            // [ANGGARAN] => 2226000000.00
            // [REALISASI] => 2226000000.00