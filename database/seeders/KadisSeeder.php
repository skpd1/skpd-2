<?php

namespace Database\Seeders;

use App\Models\Kadis;
use Illuminate\Database\Seeder;

class KadisSeeder extends Seeder
{
  public function run()
  {
    Kadis::create([
      'nm_admin' => 'Admin 1 ',
      'email' => 'admin@xmlple.com',
      'kode_lv' => '1',
      'password' => bcrypt('12345')
    ]);
    Kadis::create([
      'nm_admin' => 'Admin 2',
      'email' => 'pertanian@xmlple.com',
      'kode_lv' => '2',
      'password' => bcrypt('12345')
    ]);
    Kadis::create([
      'nm_admin' => 'Admin 3',
      'email' => 'perikanan@xmlple.com',
      'kode_lv' => '3',
      'password' => bcrypt('12345')
    ]);
    Kadis::create([
      'nm_admin' => 'Admin 4',
      'email' => 'sosial@xmlple.com',
      'kode_lv' => '4',
      'password' => bcrypt('12345')
    ]);
    Kadis::create([
      'nm_admin' => 'Super Admin',
      'email' => 'adminl@xmlple.com',
      'kode_lv' => '5',
      'password' => bcrypt('12345')
    ]);
  }
}
