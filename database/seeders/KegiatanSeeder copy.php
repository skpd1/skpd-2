<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class KegiatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kegiatans')->insert([
            'kode_kegiatan' => '1867_',
            'nm_kegiatan' => 'Penyediaan Jasa Penunjang Urusan Pemerintahan Daerah',
            'kode_program' => '2.08.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('kegiatans')->insert([
            'kode_kegiatan' => '1659_',
            'nm_kegiatan' => 'Pengendalian Pelaksanaan Penanaman Modal yang menjadi Kewenangan Daerah Kabupaten/Kota',
            'kode_program' => '2.01.',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
