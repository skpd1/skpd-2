<?php

namespace Database\Seeders;

use App\Models\Faker;
use Illuminate\Database\Seeder;

class FakerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Faker::factory(10)->create();
    }
}
