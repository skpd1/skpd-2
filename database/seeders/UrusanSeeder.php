<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UrusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('urusans')->insert([
            'kode_urusan' => '71_',
            'nm_urusan' => 'Urusan Pemerintahan Bidang Koperasi, Usaha Kecil, Dan Menengah',
            'unit_key' => '345_',
            'nm_skpd' => 'Dinas',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('urusans')->insert([
            'kode_urusan' => '55_',
            'nm_urusan' => 'Urusan Pemerintahan Bidang Pendidikan',
            'unit_key' => '345_',
            'nm_skpd' => 'Dinas',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
