<?php

namespace Database\Seeders;

use Illuminate\Support\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SkpdSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('skpds')->insert([
            'lv_id' => '1',
            'unit_key' => '389_',
            'kode_skpd' => '8.01.0.00.0.00.01.',
            'nm_skpd' => 'Kantor Kesatuan Bangsa Dan Politik',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('skpds')->insert([
            'lv_id' => '2',
            'unit_key' => '361_',
            'kode_skpd' => '2.17.0.00.0.00.01.',
            'nm_skpd' => 'Dinas Koperasi, Usaha Kecil, Dan Menengah',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
        DB::table('skpds')->insert([
            'lv_id' => '5',
            'unit_key' => '349_',
            'kode_skpd' => '1.03.1.04.0.00.02.',
            'nm_skpd' => 'Dinas Pekerjaan Umum Dan Penataan Ruang',
            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
            'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
        ]);
    }
}
