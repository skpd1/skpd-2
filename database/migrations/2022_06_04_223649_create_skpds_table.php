<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSkpdsTable extends Migration
{

    public function up()
    {
        Schema::create('skpds', function (Blueprint $table) {
            $table->id();
            $table->string('unit_key');
            $table->string('kode_skpd');
            $table->string('nm_skpd');
            $table->string('lv_id');
            $table->string('type');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('skpds');
    }
}
