<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDataTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data', function (Blueprint $table) {
            $table->id();
            $table->string('unitkey');
            $table->string('kdunit');
            $table->string('skpd');
            $table->string('idurusan');
            $table->string('idprgrm');
            $table->string('kdprgrm');
            $table->string('nmprgrm');
            $table->string('idkeg');
            $table->string('nukeg');
            $table->string('nmkeg');
            $table->string('idsubkeg');
            $table->string('kdsubkeg');
            $table->string('nmsubkeg');
            $table->string('mtgkey');
            $table->string('kdper');
            $table->string('nmper');
            $table->string('anggaran');
            $table->string('realisasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data');
    }
}
