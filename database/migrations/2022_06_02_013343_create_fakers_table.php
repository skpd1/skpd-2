<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFakersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fakers', function (Blueprint $table) {
            $table->id();
            $table->string('unitkey');
            $table->string('kdunit');
            $table->string('skpd');
            $table->string('id_urusan');
            $table->string('id_program');
            $table->string('kd_program');
            $table->string('nm_program');
            $table->string('id_keg');
            $table->string('nu_keg');
            $table->string('nm_keg');
            $table->string('id_subkeg');
            $table->string('kd_subkeg');
            $table->string('nm_subkeg');
            $table->string('mtgkey');
            $table->string('kd_per');
            $table->string('nm_per');
            $table->string('anggaran');
            $table->string('realisasi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fakers');
    }
}
