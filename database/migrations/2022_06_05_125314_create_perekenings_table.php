<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePerekeningsTable extends Migration
{
    public function up()
    {
        Schema::create('perekenings', function (Blueprint $table) {
            $table->id();

            $table->string('skpd_id');
            $table->string('urusan_id');
            $table->string('program_id');
            $table->string('keg_id');
            $table->string('subkeg_id');
            $table->string('rek_id');
            $table->bigInteger('anggaran');
            $table->bigInteger('realisasi');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('perekenings');
    }
}
