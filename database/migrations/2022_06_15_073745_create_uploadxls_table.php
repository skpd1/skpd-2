<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUploadxlsTable extends Migration
{
    public function up()
    {
        Schema::create('uploadxls', function (Blueprint $table) {
            $table->id();

            // SKPD
            $table->string('unitkey');
            $table->string('kdunit');
            $table->string('skpd');

            // Urusan
            $table->string('idurusan');

            // Program
            $table->string('idprgrm');
            $table->string('kdprgrm');
            $table->string('nmprgrm');

            // Kegiatan
            $table->string('idkeg');
            $table->string('nukeg');
            $table->string('nmkeg');

            // Subkegiatan
            $table->string('idsubkeg');
            $table->string('kdsubkeg');
            $table->string('nmsubkeg');

            // Perekening
            $table->string('mtgkey');
            $table->string('kdper');
            $table->string('nmper');
            $table->string('anggaran');
            $table->string('realisasi');

            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('uploadxls');
    }
}
