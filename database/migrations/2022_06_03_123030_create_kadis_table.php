<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKadisTable extends Migration
{
    public function up()
    {
        Schema::create('kadis', function (Blueprint $table) {
            $table->id();
            $table->string('nm_admin');
            $table->string('email')->unique();
            $table->string('kode_lv');
            $table->string('password');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('kadis');
    }
}
