<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUrusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('urusans', function (Blueprint $table) {
            $table->id();
            $table->string('kode_urusan');
            $table->string('nm_urusan');
            $table->foreignId('skpd_id');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('urusans');
    }
}
