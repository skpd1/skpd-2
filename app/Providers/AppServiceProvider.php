<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Blade;
use App\Models\User;

class AppServiceProvider extends ServiceProvider
{
  /**
   * Register any application services.
   *
   * @return void
   */
  public function register()
  {
    //
  }

  /**
   * Bootstrap any application services.
   *
   * @return void
   */
  public function boot()
  {

    Gate::define('admin', function (User $user) {
      return $user->is_admin == 1;
    });

    Gate::define('kepda', function (User $user) {
      return $user->is_admin == 2;
    });

    Gate::define('user', function (User $user) {
      return $user->is_admin == 0;
    });



    Blade::directive('currency', function ($expression) {
      return "Rp. <?php echo number_format($expression,0,',','.'); ?>";
    });
  }
}
