<?php

namespace App\Exports;

use App\Models\Uploadxl;
use Maatwebsite\Excel\Concerns\FromCollection;

class UploadxlExport implements FromCollection
{

    public function collection()
    {
        return Uploadxl::all();
    }
}
