<?php

namespace App\Exports;

use App\Models\Perekening;
use Maatwebsite\Excel\Concerns\FromCollection;

class PerekeningExport implements FromCollection
{
    public function collection()
    {
        return Perekening::all();
    }
}
