<?php

namespace App\Http\Controllers;

use App\Models\Skpd;
use App\Imports\SkpdImport;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class SkpdController extends Controller
{
    public function indexx(Request $request)
    {
        $data = Skpd::join('kadis', 'skpds.lv_id', '=', 'kadis.id')
            ->select([
                'skpds.*',
                'kadis.*',
            ]);


        // $ddd = DataTables::of($data)->make(true);
        // dd($data);

        // echo '<pre>';
        // print_r($data);
        // die;




        // $posts = Skpd::latest()->get();
        // echo '<pre>';
        // print_r($posts);
        // die;

        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
        }
        return view('tabel.skpd');
    }

    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'unit_key' => 'required',
                'kode_skpd' => 'required',
                'nm_skpd' => 'required',
                'lv_id' => 'required',
                'type' => 'required'
            ],
            [
                'unit_key.required' => 'Harus diisi',
                'kode_skpd.required' => 'Harus diisi',
            ]
        );
        // dd($validatedData);
        Skpd::create($validatedData);


        if ($validatedData) {
            return back()->with('success', 'Success! Skpd created');
        } else {
            return back()->with('error', 'Failed! Skpd not created');
        }
    }

    public function edit($id)
    {
        $skpd = Skpd::findOrFail($id);
        // $skpds = Skpd::latest()->get();

        return view('skpd.edit', compact('skpd'));
    }

    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'unit_key' => 'required',
            'kode_skpd' => 'required',
            'nm_skpd' => 'required',
            'lv_id' => 'required',
            'type' => 'required'
        ]);
        Skpd::whereId($id)->update($validatedData);

        return redirect('/dataskpd')->with('success', 'Game Data is successfully updated');
    }

    public function index()
    {

        // echo 'xxx';
        // die;
        $skpds = Skpd::latest()->get();
        // echo '<pre>';
        // print_r($skpds);
        // die;

        return view('tabel.skpd', compact(['skpds']));
    }


    public function importexcel(Request $request)
    {
        // $this->validate(
        //     $request,
        //     [
        //         'file' => 'required|mimes:xls,xlsx'
        //     ]
        // );

        // $path = $request->file('file')->getRealPath();
        // $data = Excel::load($path)->get();

        // dd($data);


        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('SkpdData', $namafile);

        Excel::import(new SkpdImport, \public_path('/SkpdData/' . $namafile));
        return redirect()->back();
    }

    public function ajax(Request $request)
    {
        $input = $request->all();

        if (!empty($input['query'])) {

            $data = Skpd::select(["id", "nm_skpd"])
                ->where("nm_skpd", "LIKE", "%{$input['query']}%")
                ->get();
        } else {

            $data = Skpd::select(["id", "nm_skpd"])
                ->get();
        }

        $skpds = [];

        if (count($data) > 0) {

            foreach ($data as $row) {
                $skpds[] = array(
                    "id" => $row->id,
                    "text" => $row->nm_skpd,
                );
            }
        }

        return response()->json($skpds);
    }
}
