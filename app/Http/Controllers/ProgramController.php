<?php

namespace App\Http\Controllers;

use App\Models\Program;
use Illuminate\Http\Request;
use App\Imports\ProgramImport;
use Yajra\DataTables\DataTables;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class ProgramController extends Controller
{
  public function index(Request $request)
  {
    if ($request->ajax()) {
      return DataTables::of(Program::query())->toJson();
    }

    // $ddd = DataTables::of(Program::query())->toJson();

    // echo '<pre>';
    // print_r($ddd);
    // die;
    return view('tabel.program');
  }

  public function importexcel(Request $request)
  {
    $data = $request->file('file');

    $namafile = $data->getClientOriginalName();
    $data->move('ProgramData', $namafile);
    // dd($data);

    Excel::import(new ProgramImport, \public_path('/ProgramData/' . $namafile));
    // return redirect('/skpd');
    return redirect()->back();
  }

  public function create()
  {
    //
  }

  public function store(Request $request)
  {
    //
  }

  public function show(Program $program)
  {
    //
  }

  /**
   * Show the form for editing the specified resource.
   *
   * @param  \App\Models\Program  $program
   * @return \Illuminate\Http\Response
   */
  public function edit(Program $program)
  {
    //
  }

  public function update(Request $request, Program $program)
  {
    //
  }

  public function destroy(Program $program)
  {
    //
  }
}
