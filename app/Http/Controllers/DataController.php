<?php

namespace App\Http\Controllers;

use App\Models\Data;
use Illuminate\Http\Request;

// use Illuminate\Support\Facades;

use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Http;
use Yajra\DataTables\Facades\DataTables;
// use Vinelab\Http\Client as HttpClient;



class DataController extends Controller
{

    public function index()
    {
        // $response =
        // Http::get('https://api.themoviedb.org/3/movie/550?api_key=APIKEY');
        // return response()->json($response);

        $response = Http::get('http://103.141.75.35:8090/sikdblj.php');

        // return json_encode($response, true);


        $xml_string = $response->getBody();

        $xmlObject = simplexml_load_string($xml_string);
        $jsonString = json_encode($xmlObject);
        // $jsonArray = json_decode($jsonString, true);
        return json_decode($jsonString, true);

        // echo '<pre>';
        // print_r($response);

        // return view('tabel.all', compact('response'));

        // return $response;




        // if ($request->ajax()) {
        //     return DataTables::of($response);
        // }

        // return $response;

        // echo '<pre>';
        // dd($response);



        // return view('tabel.all');
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }

    public function show(Data $data)
    {
        //
    }

    public function edit(Data $data)
    {
        //
    }

    public function update(Request $request, Data $data)
    {
        //
    }

    public function destroy(Data $data)
    {
        //
    }
}
