<?php

namespace App\Http\Controllers;

use App\Models\Relpendapatan;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Perekening;

class ChartController extends Controller
{
  public function index()
  {

    $user = auth()->user()->skpd;

    $relpendapatans = DB::table('relpendapatans')

      ->selectRaw('sum(relpendapatans.realisasi) /sum(relpendapatans.anggaran) * 100 as persen_rel')
      ->where('skpd_id', $user)
      ->get();

    // $chartdata = "";
    // foreach ($relpendapatans as $list) {
    //   $chartdata = $list->persen_rel;
    // }

    // $array['chardata'] = $chartdata;
    return view('dashboard', compact('relpendapatans'));
    // echo $chartdata;

    // echo '<pre>';
    // print_r($relpendapatans);
    // die;
  }
}
