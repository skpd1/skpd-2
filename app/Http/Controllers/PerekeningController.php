<?php

namespace App\Http\Controllers;

use App\Models\Perekening;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Imports\PerekeningImport;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PerekeningController extends Controller
{

    public function totalAnggaran()
    {
        $data = DB::table("perekenings")
            ->select(DB::raw("SUM(anggaran) as total_anggaran"))
            ->where('skpd_id', 30)
            ->get();
        print_r($data);
    }


    public function index(Request $request)
    {
        $user = auth()->user()->skpd;

        $data_user = DB::table('skpds')
            ->where('id', $user)
            ->first();

        if ($user === null) {
            $data = Perekening::join('programs', 'perekenings.program_id', '=', 'programs.id')
                ->join('skpds', 'perekenings.skpd_id', '=', 'skpds.id')
                ->join('kegiatans', 'perekenings.keg_id', '=', 'kegiatans.id')
                ->join('subkegiatans', 'perekenings.subkeg_id', '=', 'subkegiatans.id')
                ->join('belanjas', 'perekenings.rek_id', '=', 'belanjas.id')
                ->select([
                    DB::raw('perekenings.anggaran-perekenings.realisasi as sisa'),
                    'perekenings.anggaran', 'perekenings.realisasi',
                    'skpds.nm_skpd',
                    'programs.nm_program',
                    'kegiatans.nm_kegiatan',
                    'subkegiatans.nm_subkeg',
                    'belanjas.kd_per', 'belanjas.nm_per',
                ]);
        } else {
            $data = Perekening::join('programs', 'perekenings.program_id', '=', 'programs.id')
                ->join('skpds', 'perekenings.skpd_id', '=', 'skpds.id')
                ->join('kegiatans', 'perekenings.keg_id', '=', 'kegiatans.id')
                ->join('subkegiatans', 'perekenings.subkeg_id', '=', 'subkegiatans.id')
                ->join('belanjas', 'perekenings.rek_id', '=', 'belanjas.id')
                ->where('perekenings.skpd_id', $user)
                ->select([
                    DB::raw('perekenings.anggaran-perekenings.realisasi as sisa'),
                    'perekenings.anggaran', 'perekenings.realisasi',
                    'skpds.nm_skpd',
                    'programs.nm_program',
                    'kegiatans.nm_kegiatan',
                    'subkegiatans.nm_subkeg',
                    'belanjas.kd_per', 'belanjas.nm_per',
                ]);
        }
        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
        }
        return view('tabel.perekening', compact('data_user'));
    }

    public function indexx(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(Perekening::query())->toJson();
        }

        // $ddd = DataTables::of(Perekening::query())->toJson();

        // echo '<pre>';
        // print_r($ddd);
        // die;
        return view('tabel.perekening');
    }

    public function importexcel(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('PerekeningData', $namafile);
        // dd($data);

        Excel::import(new PerekeningImport, \public_path('/PerekeningData/' . $namafile));
        // return redirect('/skpd');
        return redirect()->back();
    }


    public function indexxxxxx()
    {
        return view('tabel.perekening');
    }
}
