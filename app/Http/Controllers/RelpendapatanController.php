<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Relpendapatan;
use Yajra\DataTables\DataTables;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use App\Imports\RelpendapatanImport;
use App\Models\User;
use Maatwebsite\Excel\Facades\Excel;


class RelpendapatanController extends Controller
{
    public function index(Request $request)
    {

        $user = auth()->user()->skpd;
        $data_user = DB::table('skpds')
            ->where('id', $user)
            ->first();


        if ($user === null) {
            $data = DB::table('relpendapatans')
                ->join('skpds', 'relpendapatans.skpd_id', '=', 'skpds.id')
                ->join('pendapatans', 'relpendapatans.rek_id', '=', 'pendapatans.id')
                ->select([
                    DB::raw('round(relpendapatans.realisasi /relpendapatans.anggaran * 100,2) as percent'),
                    'relpendapatans.*',
                    'skpds.unit_key as skpd_key', 'skpds.kode_skpd', 'skpds.nm_skpd',
                    'pendapatans.*',
                ]);
        } else {
            $data = Relpendapatan::join('skpds', 'relpendapatans.skpd_id', '=', 'skpds.id')
                ->join('pendapatans', 'relpendapatans.rek_id', '=', 'pendapatans.id')
                ->where('relpendapatans.skpd_id', $user)
                // ->groupBy('skpds.id')
                ->select([
                    // DB::raw('relpendapatans.anggaran-relpendapatans.realisasi as percent'),
                    DB::raw('round(relpendapatans.realisasi /relpendapatans.anggaran * 100,2) as percent'),
                    'relpendapatans.*',
                    'skpds.unit_key as skpd_key', 'skpds.kode_skpd', 'skpds.nm_skpd',
                    'pendapatans.*',
                ]);
        }

        // echo '<pre>';
        // print_r($data);
        // die;
        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
        }
        return view('tabel.relpendapatan', compact(
            'data_user',
            'data',
        ));
    }

    public function xxx(Request $request)
    {
        $user = auth()->user()->skpd;

        $data_user = DB::table('skpds')
            ->where('id', $user)
            ->first();

        $data = Relpendapatan::join('skpds', 'relpendapatans.skpd_id', '=', 'skpds.id')
            ->join('pendapatans', 'relpendapatans.rek_id', '=', 'pendapatans.id')
            ->where('relpendapatans.skpd_id', $user)
            ->select([
                'relpendapatans.*',
                'skpds.unit_key as skpd_key', 'skpds.kode_skpd', 'skpds.nm_skpd',
                'pendapatans.*',
            ]);

        $data_admin = Relpendapatan::join('skpds', 'relpendapatans.skpd_id', '=', 'skpds.id')
            ->join('pendapatans', 'relpendapatans.rek_id', '=', 'pendapatans.id')
            ->where('skpds.lv_id', 3)
            ->select([
                'relpendapatans.*',
                'skpds.unit_key as skpd_key', 'skpds.kode_skpd', 'skpds.nm_skpd',
                'pendapatans.*',
            ]);

        if ($request->ajax()) {
            return DataTables::of($data_admin)->make(true);
        }
        return view('tabel.relpendapatan', compact(
            'data_admin',
            'data_user',
            'data',
        ));
    }

    public function importexcel(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('RelpendapatanData', $namafile);
        // dd($data);

        Excel::import(new RelpendapatanImport, \public_path('/RelpendapatanData/' . $namafile));
        return redirect()->back();
    }

    public function macroTest(Request $request)
    {
        $dekan = User::latest();

        foreach ($dekan as $row) {

            $row = [];

            // $row->nip;
        }
    }
}
