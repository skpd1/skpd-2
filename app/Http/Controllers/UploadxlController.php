<?php

namespace App\Http\Controllers;

use App\Models\Uploadxl;
use App\Exports\UploadxlExport;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class UploadxlController extends Controller
{
    public function index(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(Uploadxl::query())->toJson();
        }
        return view('tabel.uploadxl');
    }

    public function exportxl()
    {
        return Excel::download(new UploadxlExport, 'datadariurl.xls');
    }
}
