<?php

namespace App\Http\Controllers;

use App\Models\Faker;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Yajra\DataTables\Facades\DataTables;

class FakerController extends Controller
{
    public function index()
    {
        $posts = Faker::latest()->get();

        return $posts;
    }

    // table serverside
    public function serverside(Request $request)
    {
        if ($request->ajax()) {
            return DataTables::of(Faker::query())->toJson();
        }
        return view('datafakeserver');

        // $dd = DataTables::of(Faker::query())->toJson();
        // dd($dd);
    }

    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
