<?php

namespace App\Http\Controllers;

use App\Models\Urusan;
use App\Models\Skpd;
use Illuminate\Http\Request;
use App\Imports\UrusanImport;
use Yajra\DataTables\DataTables;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use PhpParser\Node\Stmt\Echo_;

class UrusanController extends Controller
{

    public function index(Request $request)
    {
        // echo 'xx';
        // die;
        $data = Urusan::join('skpds', 'urusans.skpd_id', '=', 'skpds.id')
            ->select([
                'urusans.*',
                'skpds.*',
            ]);

        // echo '<pre>';
        // print_r($data);
        // die;

        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
            // return DataTables::of(Urusan::query())->toJson();
        }
        return view('tabel.urusan');
    }

    public function importexcel(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('UrusanData', $namafile);
        // dd($data);

        Excel::import(new UrusanImport, \public_path('/UrusanData/' . $namafile));
        // return redirect('/skpd');
        return redirect()->back();
    }

    public function test()
    {

        // $data = Skpd::latest()->get();
        $data = Skpd::where('unit_key', '1435_')->get();

        // dd($data);

        echo '<pre>';
        print_r($data);
        die;

        // $pagination  = 5;
        // $articles    = Article::when($request->keyword, function ($query) use ($request) {
        //     $query
        //     ->where('title', 'like', "%{$request->keyword}%");
        // })->orderBy('created_at', 'desc')->paginate($pagination);

        // $articles->appends($request->only('keyword'));

        // return view('articles.index', [
        //     'title'    => 'Articles',
        //     'articles' => $articles,
        // ])->with('i', ($request->input('page', 1) - 1) * $pagination);
    }
}
