<?php

namespace App\Http\Controllers;

use App\Models\Programskpd;
use Illuminate\Http\Request;
use App\Imports\ProgramskpdImport;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;
use Yajra\DataTables\Facades\DataTables;

class ProgramskpdController extends Controller
{
    public function index(Request $request)
    {
        $data = Programskpd::join('skpds', 'programskpds.skpd_id', '=', 'skpds.id')
            ->select([
                'programskpds.*',
                'skpds.*',
            ]);

        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
            // return DataTables::of(Programskpd::query())->toJson();
        }

        // $ddd = DataTables::of(Programskpd::query())->toJson();

        // echo '<pre>';
        // print_r($ddd);
        // die;
        return view('tabel.programskpd');
    }

    public function importexcel(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('ProgramskpdData', $namafile);
        // dd($data);

        Excel::import(new ProgramskpdImport, \public_path('/ProgramskpdData/' . $namafile));
        // return redirect('/skpd');
        return redirect()->back();
    }
}
