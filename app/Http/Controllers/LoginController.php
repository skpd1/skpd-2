<?php

namespace App\Http\Controllers;

use App\Models\Login;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Validated;

class LoginController extends Controller
{
  public function index()
  {
    return view('login', [
      'judul' => 'Login'
    ]);
  }

  public function auth(Request $request)
  {
    $credentials = $request->validate([
      // 'email' => ['required', 'email'],
      'username' => ['required'],
      'password' => ['required'],
    ]);

    // return redirect()->intended('/xxxx');


    if (Auth::attempt($credentials)) {
      $request->session()->regenerate();

      return redirect('/dashboard');
    }

    return back()->with('loginError', 'Login Error');
  }

  public function logout(Request $request)
  {
    Auth::logout();

    request()->session()->invalidate();

    request()->session()->regenerateToken();

    //return redirect('/login');
    return redirect()->action([LoginController::class, 'index']);
  }
}
