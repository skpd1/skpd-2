<?php

namespace App\Http\Controllers;

use App\Models\Relpendapatan;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Models\Perekening;

class DashboardController extends Controller
{
  public function index()
  {
    $user = auth()->user()->skpd;

    $data = DB::table('skpds')
      ->where('id', $user)
      ->first();

    // realisasi_belanja  
    $relbelanjas_skpd_keg = DB::table('perekenings')

      ->selectRaw('sum(perekenings.anggaran) as sum_anggaran_keg,count(perekenings.anggaran) as total_keg_direk_belanja,perekenings.keg_id')
      ->where('skpd_id', $user)
      ->groupBy('perekenings.keg_id')
      ->get();

    $relbelanjas_skpd_subkeg = DB::table('perekenings as a')
      ->join('kegiatans as b', 'a.keg_id', '=', 'b.id')
      ->join('subkegiatans as c', 'a.subkeg_id', '=', 'c.id')
      ->join('belanjas as d', 'a.rek_id', '=', 'd.id')
      ->join('programs as e', 'a.program_id', '=', 'e.id')
      ->selectRaw('b.nm_kegiatan,
                  c.nm_subkeg,
                  d.nm_per,
                  e.nm_program,
                  sum(a.anggaran) as sum_anggaran_subkeg,
                  count(a.anggaran) as total_subkeg_direk_belanja')
      ->where('a.skpd_id', $user)
      ->groupBy('b.nm_kegiatan')
      ->groupBy('c.nm_subkeg')
      ->groupBy('d.nm_per')
      ->groupBy('e.nm_program')
      ->distinct('nm_program')
      ->get();


    if ($user === null) {
      $pdptnanggaran = Relpendapatan::sum('anggaran');
      $pdptnrealisasi = Relpendapatan::sum('realisasi');

      $blnjanggaran = Perekening::sum('anggaran');
      $blnjrealisasi = Perekening::sum('realisasi');

      $relpendapatans = DB::table('relpendapatans')

        ->selectRaw('round(sum(relpendapatans.realisasi) /sum(relpendapatans.anggaran) * 100,2) as persen_rel')
        ->get();

      // yg telah realisasi
      foreach ($relpendapatans as $list) {
        $chartdata = $list->persen_rel;
      }

      $relpendapatans_x = DB::table('relpendapatans')

        ->selectRaw('round((sum(relpendapatans.anggaran)-sum(relpendapatans.realisasi)) /sum(relpendapatans.anggaran) * 100,2) as persen_rel')
        ->get();

      // yg belum realisasi
      foreach ($relpendapatans_x as $list) {
        $chartdata_x = $list->persen_rel;
      }

      $relpendapatan_blm_real = DB::table('relpendapatans')

        ->selectRaw('sum(relpendapatans.anggaran)-sum(relpendapatans.realisasi) as blm_rel')
        ->get();

      foreach ($relpendapatan_blm_real as $list) {
        $data_blm_rel_pdptn = $list->blm_rel;
      }

      //belanjas
      $relbelanjas = DB::table('perekenings')

        ->selectRaw('round(sum(perekenings.realisasi) /sum(perekenings.anggaran) * 100,2) as persen_rel')
        ->get();

      // yg telah realisasi
      foreach ($relbelanjas as $list) {
        $chartbelanja_tlh = $list->persen_rel;
      }

      // yg belum (diagram)
      $relbelanjas_blm = DB::table('perekenings')

        ->selectRaw('round((sum(perekenings.anggaran)-sum(perekenings.realisasi)) /sum(perekenings.anggaran) * 100,2) as persen_rel')
        ->get();

      foreach ($relbelanjas_blm as $list) {
        $chartbelanja_blm = $list->persen_rel;
      }

      // yg belum realisasi
      $relblnja_blm_real = DB::table('perekenings')

        ->selectRaw('sum(perekenings.anggaran)-sum(perekenings.realisasi) as blm_rel')
        ->get();

      foreach ($relblnja_blm_real as $list) {
        $data_blm_rel_blnj = $list->blm_rel;
      }

      $jml_program =  DB::table('perekenings')
        ->distinct('program_id')
        ->count('program_id');

      $jml_kegiatan =  DB::table('perekenings')
        ->distinct('keg_id')
        ->count('keg_id');

      $jml_subkegiatan =  DB::table('perekenings')
        ->distinct('subkeg_id')
        ->count('subkeg_id');
      // ------------------------------------------------------------------------------------------------------
    } else {
      $pdptnanggaran = Relpendapatan::where('skpd_id', '=', $user)->sum('anggaran');
      $pdptnrealisasi = Relpendapatan::where('skpd_id', '=', $user)->sum('realisasi');

      $blnjanggaran = Perekening::where('skpd_id', '=', $user)->sum('anggaran');
      $blnjrealisasi = Perekening::where('skpd_id', '=', $user)->sum('realisasi');

      $relpendapatans = DB::table('relpendapatans')

        ->selectRaw('round(sum(relpendapatans.realisasi) /sum(relpendapatans.anggaran) * 100,2) as persen_rel')
        ->where('skpd_id', $user)
        ->get();

      // yg telah realisasi
      foreach ($relpendapatans as $list) {
        $chartdata = $list->persen_rel;
      }

      $relpendapatans_x = DB::table('relpendapatans')

        ->selectRaw('round((sum(relpendapatans.anggaran)-sum(relpendapatans.realisasi)) /sum(relpendapatans.anggaran) * 100,2) as persen_rel')
        ->where('skpd_id', $user)
        ->get();

      // yg belum realisasi
      foreach ($relpendapatans_x as $list) {
        $chartdata_x = $list->persen_rel;
      }

      $relpendapatan_blm_real = DB::table('relpendapatans')

        ->selectRaw('sum(relpendapatans.anggaran)-sum(relpendapatans.realisasi) as blm_rel')
        ->where('skpd_id', $user)
        ->get();

      foreach ($relpendapatan_blm_real as $list) {
        $data_blm_rel_pdptn = $list->blm_rel;
      }

      //belanjas
      $relbelanjas = DB::table('perekenings')

        ->selectRaw('round(sum(perekenings.realisasi) /sum(perekenings.anggaran) * 100,2) as persen_rel')
        ->where('skpd_id', $user)
        ->get();

      // yg telah realisasi
      foreach ($relbelanjas as $list) {
        $chartbelanja_tlh = $list->persen_rel;
      }

      // yg belum (diagram)
      $relbelanjas_blm = DB::table('perekenings')

        ->selectRaw('round((sum(perekenings.anggaran)-sum(perekenings.realisasi)) /sum(perekenings.anggaran) * 100,2) as persen_rel')
        ->where('skpd_id', $user)
        ->get();

      foreach ($relbelanjas_blm as $list) {
        $chartbelanja_blm = $list->persen_rel;
      }

      // yg belum realisasi
      $relblnja_blm_real = DB::table('perekenings')

        ->selectRaw('sum(perekenings.anggaran)-sum(perekenings.realisasi) as blm_rel')
        ->where('skpd_id', $user)
        ->get();

      foreach ($relblnja_blm_real as $list) {
        $data_blm_rel_blnj = $list->blm_rel;
      }

      $jml_program =  DB::table('perekenings')
        ->where('skpd_id', $user)
        ->distinct('program_id')
        ->count('program_id');

      $jml_kegiatan =  DB::table('perekenings')
        ->where('skpd_id', $user)
        ->distinct('keg_id')
        ->count('keg_id');

      $jml_subkegiatan =  DB::table('perekenings')
        ->where('skpd_id', $user)
        ->distinct('subkeg_id')
        ->count('subkeg_id');
    }

    // echo '<pre>';
    // print_r($relpendapatan_blm_real);
    // die;


    //dasboardAdmin
    //pendaptan --------------------------------------------------------------------------------
    //total_pendapatan 
    $pdptnanggaran_admin = DB::table('relpendapatans as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.anggaran) as total_pdptn_angg_all_skpd')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($pdptnanggaran_admin as $row) {
      $pdptnanggaran_admin_data = $row->total_pdptn_angg_all_skpd;
    }

    //blm_realisasi_pdptn_admin
    $relpendapatans_admin_blm = DB::table('relpendapatans as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.anggaran)-sum(a.realisasi) as blm_rel')
      ->where('b.lv_id', 3)
      ->get();
    foreach ($relpendapatans_admin_blm as $row) {
      $relpendapatans_admin_blm_data = $row->blm_rel;
    }

    //tlh_realisasi_pdptn_admin
    $relpendapatans_admin_tlh = DB::table('relpendapatans as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.realisasi) as tlh_rel')
      ->where('b.lv_id', 3)
      ->get();
    foreach ($relpendapatans_admin_tlh as $row) {
      $relpendapatans_admin_tlh_data = $row->tlh_rel;
    }

    //piechart_tlh_realisasi_pdptn
    $relpendapatans_admin_tlh_chart = DB::table('relpendapatans as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('round(sum(a.realisasi) /sum(a.anggaran) * 100,2) as persen_rel')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($relpendapatans_admin_tlh_chart as $row) {
      $chart_relpendapatans_tlh_admin = $row->persen_rel;
    }

    //piechart_blm_realisasi_pdptn
    $relpendapatans_admin_blm_chart = DB::table('relpendapatans as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('round((sum(a.anggaran)-sum(a.realisasi)) /sum(a.anggaran) * 100,2) as persen_rel')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($relpendapatans_admin_blm_chart as $row) {
      $chart_relpendapatans_blm_admin = $row->persen_rel;
    }

    //blnja --------------------------------------------------------------------------------
    //total_blnja 
    $blnj_anggaran_admin = DB::table('perekenings as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.anggaran) as total_blnja_angg_all_skpd')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($blnj_anggaran_admin as $row) {
      $blnja_anggaran_admin_data = $row->total_blnja_angg_all_skpd;
    }

    //blm_realisasi_blnja_admin
    $perekenings_admin_blm = DB::table('perekenings as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.anggaran)-sum(a.realisasi) as blm_rel')
      ->where('b.lv_id', 3)
      ->get();
    foreach ($perekenings_admin_blm as $row) {
      $perekenings_admin_blm_data = $row->blm_rel;
    }

    //tlh_realisasi_blnja_admin
    $perekenings_admin_tlh = DB::table('perekenings as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('sum(a.realisasi) as tlh_rel')
      ->where('b.lv_id', 3)
      ->get();
    foreach ($perekenings_admin_tlh as $row) {
      $perekenings_admin_tlh_data = $row->tlh_rel;
    }

    //piechart_tlh_realisasi_pdptn
    $perekenings_admin_tlh_chart = DB::table('perekenings as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('round(sum(a.realisasi) /sum(a.anggaran) * 100,2) as persen_rel')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($perekenings_admin_tlh_chart as $row) {
      $chart_perekenings_tlh_admin = $row->persen_rel;
    }

    //piechart_blm_realisasi_pdptn
    $perekenings_admin_blm_chart = DB::table('perekenings as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->selectRaw('round((sum(a.anggaran)-sum(a.realisasi)) /sum(a.anggaran) * 100,2) as persen_rel')
      ->where('b.lv_id', 3)
      ->get();

    foreach ($perekenings_admin_blm_chart as $row) {
      $chart_perekenings_blm_admin = $row->persen_rel;
    }


    // program_admin
    $jml_program_admin =  DB::table('perekenings  as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->where('b.lv_id', 3)
      ->distinct('program_id')
      ->count('program_id');

    // keg_admin
    $jml_kegiatan_admin =  DB::table('perekenings  as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->where('b.lv_id', 3)
      ->distinct('keg_id')
      ->count('keg_id');

    // subkeg_admin
    $jml_subkegiatan_admin =  DB::table('perekenings  as a')
      ->join('skpds as b', 'a.skpd_id', '=', 'b.id')
      ->where('b.lv_id', 3)
      ->distinct('subkeg_id')
      ->count('subkeg_id');




    // echo '<pre>';
    // print_r($user);
    // die;



    return view('dashboard', compact(
      'data',
      'pdptnanggaran',
      'pdptnrealisasi',
      'blnjanggaran',
      'blnjrealisasi',
      'relbelanjas_skpd_keg',
      'relbelanjas_skpd_subkeg',
      'jml_program',
      'jml_kegiatan',
      'jml_subkegiatan',
      'chartdata',
      'chartdata_x',
      'data_blm_rel_pdptn',
      'chartbelanja_tlh',
      'chartbelanja_blm',
      'data_blm_rel_blnj',
      'pdptnanggaran_admin_data',
      'chart_relpendapatans_tlh_admin',
      'chart_relpendapatans_blm_admin',
      'relpendapatans_admin_blm_data',
      'relpendapatans_admin_tlh_data',

      'blnja_anggaran_admin_data',
      'chart_perekenings_tlh_admin',
      'chart_perekenings_blm_admin',
      'perekenings_admin_blm_data',
      'perekenings_admin_tlh_data',

      'jml_program_admin',
      'jml_kegiatan_admin',
      'jml_subkegiatan_admin'

    ));





    // echo '<pre>';
    // print_r($list_keg_relbelanja);
    // die;


    // $list_keg_relbelanja = [];
    // $blnjanggaran_keg = Perekening::where([
    //   // ['skpd_id', '=', $user],
    //   ['keg_id', '=', $list_keg_relbelanja],
    // ])
    //   ->get('perekenings.keg_id');
    // // ->sum('anggaran');


    //realisasi_belanja  
    // $relbelanjas_skpd = DB::table('perekenings')
    //   ->where('skpd_id', $user)
    //   ->where('keg_id', '589')
    //   // ->sum('anggaran')
    //   ->get();








    // $sum_keg = Perekening::join('kegiatans', 'perekenings.keg_id', '=', 'kegiatans.id')
    //   ->where('perekenings.skpd_id', $user)
    //   ->where('perekenings.keg_id', '589')
    //   ->selectraw('sum(anggaran) as total_keg_anggaran_belanja')
    //   ->selectraw('sum(realisasi) as total_keg_realisasi_belanja')
    //   ->get('perekenings.*',);



    // $sum_anggaran_relbelanjas_keg 

    // $id_user = '11';
    // $offers = Offer::join('clients', 'clients.id', '=', 'offers.client')
    //      ->leftJoin('offer_items','offer_items.id_offer', '=', 'offers.id')
    //      ->selectRaw(' sum(IFNULL(offer_items.amount,0)) as suma, clients.name, offers.*')
    //      ->where('offers.id_user', $id_user)
    //      ->groupBy('offers.id')
    //      ->Orderby('offers.id')
    //      ->get();



    // // list_keg_relbelanja
    // foreach ($relbelanjas_skpd as $row) {
    //   $list_keg_relbelanja[] =  $row->keg_id;
    // }
    // $list_keg = DB::table('kegiatans')
    //   ->where('id', $list_keg_relbelanja)
    //   ->get();

    // $blnjanggaran_allkeg = Perekening::where('skpd_id', '=', $user)->sum('anggaran');

    // echo '<pre>';
    // print_r($sum_keg);
    // die;


    //realisasi_pendapatan  
    // $relpendapatan = DB::table('relpendapatans')
    //   ->where('skpd_id', $user)
    //   ->get();




    // $listkeg = DB::table("perekenings")
    //   ->join('kegiatans', 'perekenings.keg_id', '=', 'kegiatans.id')
    //   ->where('skpd_id', $user)
    //   ->get([
    //     'kegiatans.nm_kegiatan as nm_keg',
    //   ]);

    // $listsubkeg = DB::table("perekenings")
    //   ->join('subkegiatans', 'perekenings.subkeg_id', '=', 'subkegiatans.id')
    //   ->where('subkeg_id', $user)
    //   ->get([
    //     'kegiatans.nm_kegiatan as nm_keg',
    //   ]);

    // $listsubkeg = DB::table("subkegiatans")
    //   ->join('skpds', 'subkegiatans.skpd_id', '=', 'skpds.id')
    //   ->where('skpd_id', $user)
    //   ->get([
    //     'subkegiatans.*',
    //   ]);

    // //pendapatan
    // $pdptnanggaran = Relpendapatan::where('skpd_id', '=', $user)->sum('anggaran');
    // $pdptnrealisasi = Relpendapatan::where('skpd_id', '=', $user)->sum('realisasi');
    // //belanja
    // $blnjanggaran = Perekening::where('skpd_id', '=', $user)->sum('anggaran');
    // $blnjrealisasi = Perekening::where('skpd_id', '=', $user)->sum('realisasi');



    // return view('dashboard', compact(
    //   'data',
    //   'pdptnanggaran',
    //   'pdptnrealisasi',
    //   'blnjanggaran',
    //   'blnjrealisasi'
    // ));
  }
}
