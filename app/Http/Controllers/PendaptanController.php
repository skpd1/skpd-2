<?php

namespace App\Http\Controllers;

use App\Models\Skpd;
use App\Models\Perekening;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Imports\PerekeningImport;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;

class PerekeningController extends Controller
{
  public function index(Request $request)
  {
    $data = Perekening::join('skpds', 'perekenings.skpd_id', '=', 'skpds.id')
      ->join('urusans', 'perekenings.urusan_id', '=', 'urusans.id')
      ->join('programs', 'perekenings.program_id', '=', 'programs.id')
      ->join('kegiatans', 'perekenings.keg_id', '=', 'kegiatans.id')
      ->join('subkegiatans', 'perekenings.subkeg_id', '=', 'subkegiatans.id')
      ->join('belanjas', 'perekenings.rek_id', '=', 'belanjas.mtg_key')
      ->join('pendapatans', 'perekenings.rek_id', '=', 'pendapatans.mtg_key')
      ->get([
        // ->select([
        'perekenings.*',
        'skpds.unit_key as skpd_key', 'skpds.kode_skpd', 'skpds.nm_skpd',
        'urusans.*',
        'programs.unit_key as program_key', 'programs.nm_program', 'programs.nu_program', 'programs.id_program',
        'kegiatans.*',
        'subkegiatans.*',
        'belanjas.*',
        'pendapatans.*',
      ]);


    // $zz = Perekening::where('skpd_id', '=', 2)->sum('anggaran');




    if ($request->ajax()) {
      return DataTables::of($data)->make(true);
      // return DataTables::of(Perekening::query())->toJson();
      // return DataTables::of($data)->make(true);
    }
    return view('tabel.relpendapatan');
  }



  public function importexcel(Request $request)
  {
    $data = $request->file('file');

    $namafile = $data->getClientOriginalName();
    $data->move('PerekeningData', $namafile);
    // dd($data);

    Excel::import(new PerekeningImport, \public_path('/PerekeningData/' . $namafile));
    // return redirect('/skpd');
    return redirect()->back();
  }
}
