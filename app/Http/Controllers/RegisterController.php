<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Hash;

class RegisterController extends Controller
{
  public function index()
  {
    return view('reg', [
      'Judul' => 'Daftar',
      'active' => 'register',
    ]);
  }

  public function store(Request $request)
  {
    //return request()->all();
    // echo '<pre>';
    // print_r($request);
    // die;
    $validatedData = $request->validate([
      // 'name' => 'required|max:255',
      'username' => 'required|min:5|max:255|unique:users',
      'password' => 'required|min:5',
      // 'skpd' => 'required',
      // 'nip' => 'required'
    ]);

    // dd($validatedData);

    $validatedData['password'] = Hash::make($validatedData['password']);

    User::create($validatedData);
    return redirect('/login')->with('success', 'Registration Succesfull! Please Login');
  }
}
