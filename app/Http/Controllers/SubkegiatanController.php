<?php

namespace App\Http\Controllers;

use App\Models\Subkegiatan;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use App\Imports\SubkegiatanImport;
use Illuminate\Routing\Controller;
use Maatwebsite\Excel\Facades\Excel;

class SubkegiatanController extends Controller
{
    public function index(Request $request)
    {
        $data = Subkegiatan::join('skpds', 'subkegiatans.skpd_id', '=', 'skpds.id')
            ->select([
                'subkegiatans.*',
                'skpds.*',
            ]);

        if ($request->ajax()) {
            return DataTables::of($data)->make(true);
            // return DataTables::of(Subkegiatan::query())->toJson();
        }

        // $ddd = DataTables::of(Subkegiatan::query())->toJson();

        // echo '<pre>';
        // print_r($ddd);
        // die;
        return view('tabel.subkegiatan');
    }

    public function importexcel(Request $request)
    {
        $data = $request->file('file');

        $namafile = $data->getClientOriginalName();
        $data->move('SubkegiatanData', $namafile);
        // dd($data);

        Excel::import(new SubkegiatanImport, \public_path('/SubkegiatanData/' . $namafile));
        return redirect()->back();
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        //
    }


    public function show(Subkegiatan $subkegiatan)
    {
        //
    }

    public function edit(Subkegiatan $subkegiatan)
    {
        //
    }

    public function update(Request $request, Subkegiatan $subkegiatan)
    {
        //
    }

    public function destroy(Subkegiatan $subkegiatan)
    {
        //
    }
}
