<?php

namespace App\Http\Controllers;

use App\Models\Skpd;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index()
    {

        $users = User::join('skpds', 'users.skpd', '=', 'skpds.id')
            ->get([
                'users.*',
                'skpds.*'
            ]);

        $skpds = DB::table('skpds')->whereIn('lv_id', array(3))->get();


        // dd($skpds);

        return view('user-data', compact('users', 'skpds'));
    }

    // public function ajax(Request $request)
    // {
    //     $search = $request->search;
    //     // $search = 'dinas';

    //     if ($search == '') {
    //         $employees = Skpd::orderby('nm_skpd', 'asc')->select('id', 'nm_skpd')->limit(5)->get();
    //     } else {
    //         $employees = Skpd::orderby('nm_skpd', 'asc')->select('id', 'nm_skpd')->where('nm_skpd', 'like', '%' . $search . '%')->limit(5)->get();
    //     }

    //     $response = array();
    //     foreach ($employees as $employee) {
    //         $response[] = array(
    //             'id' => $employee->id,
    //             'text' => $employee->nm_skpd
    //         );
    //     }
    //     // return view('user-data', compact('response'));


    //     // dd($response);
    //     return response()->json($response);
    // }

    public function store(Request $request)
    {
        $validatedData = $request->validate(
            [
                'username' => 'required|unique:users',
                'password' => 'required',
                'name' => 'required',
                'skpd' => 'required',
                'nip' => 'required'
            ],
            [
                'username.required' => 'Harus diisi',
                'username.unique' => 'Username sudah ada',
            ]
        );
        $validatedData['password'] = Hash::make($validatedData['password']);

        // dd($validatedData);
        User::create($validatedData);


        if ($validatedData) {
            return back()->with('success', 'Success! User created');
        } else {
            return back()->with('error', 'Failed! User not created');
        }
    }

    public function show($id)
    {
        $id = 1;
        $user = DB::table('users')->where('id', $id)->first();

        // dd($user);

        // $user = User::find($id);
        // $userdata = User::latest()->get();
        // dd($userdata);
        return view('user-data', compact('user'));
    }

    public function edit($id)
    {

        $user = User::findOrFail($id);
        $skpds = Skpd::latest()->get();

        return view('user.edit', compact(['user', 'skpds']));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $validatedData = $request->validate([
            'username' => 'required',
            'password' => 'required',
            'name' => 'required',
            'skpd_id' => 'required',
            'nip' => 'required'
        ]);
        User::whereId($id)->update($validatedData);

        return redirect('/datauser')->with('success', 'Game Data is successfully updated');



        // return view('user-data');

        // dd($request->all());
        // $user = User::findOrFail($user->id);



        // $dataskpd = Skpd::all();

        // $validatedData = $request->validate(
        //     [
        //         'username' => 'required',
        //         'password' => 'required',
        //         'name' => 'required',
        //         'skpd_id' => 'required',
        //         'nip' => 'required'
        //     ],
        //     [
        //         'username.required' => 'Harus diisi',
        //     ]
        // );
        // User::whereId($id)->update($validatedData);


        // if ($validatedData) {
        //     return redirect('datauser')->with('success', 'Success! User created');
        // } else {
        //     return redirect('datauser')->with('error', 'Failed! User not created');
        // }
    }


    // public function index()
    // {
    //     if(request()->ajax()) {
    //         return datatables()->of(Company::select('*'))
    //         ->addColumn('action', 'company-action')
    //         ->rawColumns(['action'])
    //         ->addIndexColumn()
    //         ->make(true);
    //     }
    //     return view('companies');
    // }


}
