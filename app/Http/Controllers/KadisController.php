<?php

namespace App\Http\Controllers;

use App\Models\Kadis;
use Illuminate\Http\Request;

class KadisController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userdata = Kadis::latest()->get();

        return view('user-data', compact('userdata'));
        // dd($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Kadis  $kadis
     * @return \Illuminate\Http\Response
     */
    public function show(Kadis $kadis)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Kadis  $kadis
     * @return \Illuminate\Http\Response
     */
    public function edit(Kadis $kadis)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Kadis  $kadis
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Kadis $kadis)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Kadis  $kadis
     * @return \Illuminate\Http\Response
     */
    public function destroy(Kadis $kadis)
    {
        //
    }
}
