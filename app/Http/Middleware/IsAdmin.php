<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class IsAdmin
{

  public function handle(Request $request, Closure $next)
  {
    /*  if (!auth()->check() || !auth()->user()->is_admin) {
      abort(403);
    } */
    if (!auth()->check()) {
      return redirect()->guest('login');
    } elseif (!auth()->user()->is_admin) {
      //abort(403);
      return response()->view('errors.403');
    }
    return $next($request);
  }
}
