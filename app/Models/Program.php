<?php

namespace App\Models;


use App\Models\Perekening;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Program extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function perekening()
    {
        return $this->hasMany(Perekening::class); // 1 program, bisa bnyak perekening
    }
}
