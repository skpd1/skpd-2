<?php

namespace App\Models;

use App\Models\Skpd;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kadis extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function skpd()
    {
        return $this->hasMany(Skpd::class); // 1 Level kadis, bisa bnyak SKPD
    }
}
