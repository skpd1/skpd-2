<?php

namespace App\Models;

use App\Models\Perekening;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Kegiatan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function perekening()
    {
        return $this->hasMany(Perekening::class); // 1 kegiatan, bisa bnyak perekening
    }
}
