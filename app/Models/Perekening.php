<?php

namespace App\Models;

use App\Models\Skpd;
use App\Models\Urusan;
use App\Models\Program;
use App\Models\Kegiatan;
use App\Models\Subkegiatan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Perekening extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function skpd()
    {
        return $this->belongsTo(Skpd::class, 'skpd_id'); // 1  perekening, hanya 1 skpd
    }
    public function urusan()
    {
        return $this->belongsTo(Urusan::class, 'urusan_id'); // 1 perekening, hanya 1 urusan 
    }
    public function program()
    {
        return $this->belongsTo(Program::class, 'program_id'); // 1 perekening, hanya 1 program 
    }
    public function kegiatan()
    {
        return $this->belongsTo(Kegiatan::class, 'kegiatan_id'); // 1 perekening, hanya 1 kegiatan 
    }
    public function subkegiatan()
    {
        return $this->belongsTo(Subkegiatan::class, 'subkegiatan_id'); // 1 perekening, hanya 1 subkegiatan 
    }
}
