<?php

namespace App\Models;

use App\Models\Perekening;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Urusan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function perekening()
    {
        return $this->hasMany(Perekening::class); // 1 urusan, bisa bnyak perekening
    }

    public function skpd()
    {
        return $this->belongsTo(Skpd::class, 'skpd_id'); // 1 urusan, hanya 1 skpd 
    }
}
