<?php

namespace App\Models;

use App\Models\User;
use App\Models\Urusan;
use App\Models\Perekening;
use App\Models\Programskpd;
use App\Models\Subkegiatan;
use App\Models\Relpendapatan;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Skpd extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function perekening()
    {
        return $this->hasMany(Perekening::class);  // 1 skpd, bisa bnyak perekening
    }
    public function relpendapatan()
    {
        return $this->hasMany(Relpendapatan::class);  // 1 skpd, bisa bnyak relpendapatan
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id'); // 1 skpd, hanya 1 user 
    }


    // salah
    public function urusan()
    {
        return $this->hasMany(Urusan::class);  // 1 skpd, bisa bnyak urusan
    }

    public function programskpd()
    {
        return $this->hasMany(Programskpd::class);  // 1 skpd, bisa bnyak programskpd
    }

    public function subkegiatan()
    {
        return $this->hasMany(Subkegiatan::class);  // 1 skpd, bisa bnyak subkegiatan
    }
}
