<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Relpendapatan extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function skpd()
    {
        return $this->belongsTo(Skpd::class, 'skpd_id'); // 1  relpendapatan, hanya 1 skpd
    }
}
