<?php

namespace App\Models;

use App\Models\Perekening;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Subkegiatan extends Model
{
    use HasFactory;
    protected $guarded = ['id'];

    public function perekening()
    {
        return $this->hasMany(Perekening::class); // 1 subkegiatan, bisa bnyak perekening
    }

    public function skpd()
    {
        return $this->belongsTo(Skpd::class, 'skpd_id'); // 1 subkegiatan, hanya 1 skpd 
    }
}
