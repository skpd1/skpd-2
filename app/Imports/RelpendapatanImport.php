<?php

namespace App\Imports;
// realasi

use App\Models\Pendapatan;
use App\Models\Skpd;


use App\Models\Relpendapatan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithheadingRow;

class RelpendapatanImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            // SKPD
            $unitkey = $row['idskpd'];

            // Perekening
            $mtgkey = $row['idrek'];
            $anggaran = $row['anggaran'];
            $realisasi = $row['realisasi'];

            //skpd
            $unit_key_skpd = Skpd::where('unit_key', $unitkey)->get()->toArray();
            foreach ($unit_key_skpd as $row) {
                $id_skpd = $row['id'];
            }

            //pendapatan
            $pendapatans = Pendapatan::where('mtg_key', $mtgkey)->get()->toArray();
            foreach ($pendapatans as $row) {
                $id_rek = $row['id'];
            }



            Relpendapatan::create([
                'skpd_id' => $id_skpd,
                'rek_id' => $id_rek,
                'anggaran' => $anggaran,
                'realisasi' => $realisasi,
            ]);
        }
    }
}
