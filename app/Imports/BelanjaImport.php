<?php

namespace App\Imports;

use App\Models\Belanja;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithheadingRow;


class BelanjaImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Belanja([
            'mtg_key' => $row['mtg_key'],
            'kd_per' => $row['kd_per'],
            'nm_per' => $row['nm_per'],
            'kd_lv' => $row['kd_lv'],
            'type' => $row['type'],
        ]);
    }
}
