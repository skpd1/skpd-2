<?php

namespace App\Imports;

use App\Models\Program;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithheadingRow;


class ProgramImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Program([
            'id_program' => $row['id_program'],
            'unit_key' => $row['unit_key'],
            'nm_program' => $row['nm_program'],
            'nu_program' => $row['nu_program'],
        ]);
    }
}
