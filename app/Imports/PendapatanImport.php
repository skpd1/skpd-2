<?php

namespace App\Imports;

use App\Models\Pendapatan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithheadingRow;


class PendapatanImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return \Illuminate\Database\Eloquent\Model|null
     */
    public function model(array $row)
    {
        return new Pendapatan([
            'mtg_key' => $row['mtg_key'],
            'kd_per' => $row['kd_per'],
            'nm_per' => $row['nm_per'],
            'kd_lv' => $row['kd_lv'],
            'type' => $row['type'],
        ]);
    }
}
