<?php

namespace App\Imports;

use App\Models\Skpd;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithheadingRow;

use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;


class SkpdImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Skpd([
            'unit_key' => $row['unit_key'],
            'kode_skpd' => $row['kd_unit'],
            'nm_skpd' => $row['skpd'],
            'lv_id' => $row['kd_level'],
            'type' => $row['type'],
        ]);
    }
}
