<?php

namespace App\Imports;

use App\Models\Urusan;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithheadingRow;


class UrusanImport implements ToCollection, WithHeadingRow
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $unit_key_xl = $row['unit_key'];
            $unit_key_skpd = DB::table('skpds')->where('unit_key', 'LIKE', '%' . $unit_key_xl . '%')->first();

            // $unit_key_skpd = Skpd::where('unit_key', $unit_key_xl)->get()->toArray();

            // foreach ($unit_key_skpd as $row) {
            //     $id_skpd = $row['id'];
            //     $unit_skpd = $row['unit_key'];
            // }

            // echo '<pre>';
            // print_r($unit_key_skpd);
            // die;

            Urusan::create([
                'kode_urusan' => $row['urus_key'],
                'nm_urusan' => $row['urusan'],
                'skpd_id' => $unit_key_skpd->id,
                // 'nm_skpd' => $row['skpd'],
            ]);
        }
    }
}


// dibawah foreach
// $unit_key_xl = $row['unit_key'];
// $unit_key_skpd = Skpd::where('unit_key', $unit_key_xl)->get()->toArray();

// foreach ($unit_key_skpd as $row) {
//     $id_skpd = $row['id'];
//     $unit_skpd = $row['unit_key'];
// }

// if ($row['unit_key'] == $unit_skpd) {
//     $uk = $id_skpd;
// }
// // else {
// //     $uk = $unit_key_xl;
// // }

// // echo '<pre>';
// // print_r($uk);
// // die;
