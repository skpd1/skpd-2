<?php

namespace App\Imports;

use App\Models\Kegiatan;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithheadingRow;

class KegiatanImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        return new Kegiatan([
            'id_kegiatan' => $row['id_keg'],
            'nm_kegiatan' => $row['nm_keg'],
            'nu_kegiatan' => $row['nu_keg'],
        ]);
    }
}
