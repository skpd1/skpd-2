<?php

namespace App\Imports;
// realasi

use App\Models\Belanja;
use App\Models\Kegiatan;
use App\Models\Skpd;

// use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Perekening;
use App\Models\Program;
use App\Models\Subkegiatan;
use App\Models\Urusan;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithheadingRow;

class PerekeningImport implements ToCollection, WithHeadingRow
{
    public function collectionxxxx(Collection $rows)
    {
        foreach ($rows as $row) {

            // SKPD
            $unitkey = $row['unitkey'];
            $kdunit = $row['kdunit'];
            $skpd = $row['skpd'];

            // Urusan
            $idurusan = $row['idurusan'];

            // Program
            $idprgrm = $row['idprgrm'];
            $kdprgrm = $row['kdprgrm'];
            $nmprgrm = $row['nmprgrm'];

            // Kegiatan
            $idkeg = $row['idkeg'];
            $nukeg = $row['nukeg'];
            $nmkeg = $row['nmkeg'];

            // Subkegiatan
            $idsubkeg = $row['idsubkeg'];
            $kdsubkeg = $row['kdsubkeg'];
            $nmsubkeg = $row['nmsubkeg'];

            // Perekening
            $mtgkey = $row['mtgkey'];
            $kdper = $row['kdper'];
            $nmper = $row['nmper'];
            $anggaran = $row['anggaran'];
            $realisasi = $row['realisasi'];



            // $unit_key_xl = $row['unit_key'];
            // $xx = DB::table('skpds')->where('unit_key', 'LIKE', '%' . $unit_key_xl . '%')->first();

            $unit_key_skpd = Skpd::where('unit_key', $unitkey)->get()->toArray();
            $urusan = Urusan::where('kode_urusan', $idurusan)->get()->toArray();
            $program = Program::where('id_program', $idprgrm)->get()->toArray();
            $kegiatan = Kegiatan::where('id_kegiatan', $idkeg)->get()->toArray();
            $subkegiatan = Subkegiatan::where('kode_subkeg', $idsubkeg)->get()->toArray();

            //skpd
            foreach ($unit_key_skpd as $row) {
                $id_skpd = $row['id'];
                // $unit_skpd = $row['unit_key'];
            }

            // echo '<pre>';
            // print_r($id_skpd);
            // die;
            //urusan
            foreach ($urusan as $row) {
                $id_urusan = $row['id'];
            }
            //program
            foreach ($program as $row) {
                $id_program = $row['id'];
            }
            //kegiatan
            foreach ($kegiatan as $row) {
                $id_kegiatan = $row['id'];
            }
            //subkegiatan
            foreach ($subkegiatan as $row) {
                $id_subkegiatan = $row['id'];
            }

            // $dp = [
            //     'skpd_id' => $id_skpd,
            //     'urusan_id' => $id_urusan,
            //     'program_id' => $id_program,
            //     'kegiatan_id' => $id_kegiatan,
            //     'subkegiatan_id' => $id_subkegiatan,

            //     // Perekening
            //     'nm_mtgkey' => $mtgkey,
            //     'kode_per' => $kdper,
            //     'nm_per' => $nmper,
            //     'anggaran' => $anggaran,
            //     'realisasi' => $realisasi,
            // ];

            // dd($dp);
            // echo '<pre>';
            // print_r($id_skpd);
            // die;

            Perekening::create([
                // 'id_program' => $id_program,
                // 'nm_program' => $nm_program,
                // 'skpd_id' => $id_skpd,

                'skpd_id' => $id_skpd,
                'urusan_id' => $id_urusan,
                'program_id' => $id_program,
                'kegiatan_id' => $id_kegiatan,
                'subkegiatan_id' => $id_subkegiatan,
                // 'kd_subkeg' => $kdsubkeg,

                // Perekening
                'nm_mtgkey' => $mtgkey,
                'kode_per' => $kdper,
                'nm_per' => $nmper,
                'anggaran' => $anggaran,
                'realisasi' => $realisasi,
            ]);
        }
    }

    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {

            // SKPD
            $skpd_id = $row['idskpd'];

            // Urusan
            $urusan_id = $row['idurusan'];

            // Program
            $program_id = $row['idprgrm'];

            // Kegiatan
            $keg_id = $row['idkeg'];

            // Subkegiatan
            $subkeg_id = $row['idsubkeg'];

            // Perekening
            $rek_id = $row['idrek'];

            //anggaran
            $anggaran = $row['anggaran'];
            $realisasi = $row['realisasi'];

            $unit_key_skpd = Skpd::where('unit_key', $skpd_id)->get()->toArray();
            $urusan = Urusan::where('kode_urusan', $urusan_id)->get()->toArray();
            $program = Program::where('id_program', $program_id)->get()->toArray();
            $kegiatan = Kegiatan::where('id_kegiatan', $keg_id)->get()->toArray();
            $subkegiatan = Subkegiatan::where('kode_subkeg', $subkeg_id)->get()->toArray();
            $belanja = Belanja::where('mtg_key', $rek_id)->get()->toArray();

            //skpd
            foreach ($unit_key_skpd as $row) {
                $id_skpd = $row['id'];
            }

            //urusan
            foreach ($urusan as $row) {
                $id_urusan = $row['id'];
            }
            //program
            foreach ($program as $row) {
                $id_program = $row['id'];
            }
            //kegiatan
            foreach ($kegiatan as $row) {
                $id_kegiatan = $row['id'];
            }
            //subkegiatan
            foreach ($subkegiatan as $row) {
                $id_subkegiatan = $row['id'];
            }
            //subkegiatan
            foreach ($belanja as $row) {
                $id_rek = $row['id'];
            }

            Perekening::create([
                'skpd_id' => $id_skpd,
                'urusan_id' => $id_urusan,
                'program_id' => $id_program,
                'keg_id' => $id_kegiatan,
                'subkeg_id' => $id_subkegiatan,
                'rek_id' => $id_rek,
                'anggaran' => $anggaran,
                'realisasi' => $realisasi,
            ]);
        }
    }
}
