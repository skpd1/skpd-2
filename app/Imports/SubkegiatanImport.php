<?php

namespace App\Imports;

use App\Models\Skpd;
use App\Models\Subkegiatan;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithheadingRow;

class SubkegiatanImport implements WithHeadingRow, ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $kode_subkeg = $row['kd_subkeg'];
            $nm_subkeg  = $row['nm_subkeg'];

            $unit_key_xl = $row['unit_key'];
            $unit_key_skpdx = Skpd::where('unit_key', $unit_key_xl)->get()->toArray();


            foreach ($unit_key_skpdx as $row) {
                $id_skpd = $row['id'];
            }

            Subkegiatan::create([
                'unit_key' => $unit_key_xl,
                'kode_subkeg' => $kode_subkeg,
                'nm_subkeg' => $nm_subkeg,
                'skpd_id' => $id_skpd,
            ]);
        }
    }
}
