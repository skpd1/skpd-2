<?php

namespace App\Imports;

use App\Models\Skpd;
use App\Models\Programskpd;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithheadingRow;


class ProgramskpdImport implements WithHeadingRow, ToCollection
{
    public function collection(Collection $rows)
    {
        foreach ($rows as $row) {
            $id_program = $row['id_program'];
            $nm_program = $row['nm_program'];

            $unit_key_xl = $row['unit_key'];
            $xx = DB::table('skpds')->where('unit_key', 'LIKE', '%' . $unit_key_xl . '%')->first();

            $unit_key_skpdx = Skpd::where('unit_key', $unit_key_xl)->get()->toArray();


            foreach ($unit_key_skpdx as $row) {
                $id_skpd = $row['id'];
                $unit_skpd = $row['unit_key'];
            }

            Programskpd::create([
                'id_program' => $id_program,
                'nm_program' => $nm_program,
                'skpd_id' => $id_skpd,
            ]);
        }
    }
}
